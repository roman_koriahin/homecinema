﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Video.aspx.cs" Inherits="Video" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 style="text-align: center;"><%: videoName %></h1>
    <%
        if (Request.Browser.IsMobileDevice)
        {
    %>
    <table border="1">
        <tr>
            <td>
                <video width="100%" controls="">
                    <source src="<%: videoSource %>" type="video/mp4">
                </video>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin-left: 2px;">Тут описание</div>
            </td>
        </tr>
    </table>
    <%
        }
        else
        {
    %>
    <table border="1" style="width: 80%; margin: auto;">
        <tr>
            <td style="text-align: center;">
                <video controls="" style="height: 300px;">
                    <source src="<%: videoSource %>" type="video/mp4">
                </video>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin-left: 2px;">Тут описание</div>
            </td>
        </tr>
    </table>
    <%
        }
    %>
</asp:Content>
