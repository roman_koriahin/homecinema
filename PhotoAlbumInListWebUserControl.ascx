﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhotoAlbumInListWebUserControl.ascx.cs" Inherits="PhotoAlbumInListWebUserControl" %>

<h1>
    <a href="/<%: lang %>/Photos/<%: photoAlbumName %>/"><%: photoAlbumName.Replace("_", ".") %></a>
</h1>
