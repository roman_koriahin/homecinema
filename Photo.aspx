﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Photo.aspx.cs" Inherits="Photo" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <table border="1" style="width: 100%">
        <tr>
            <td style="width: 50px">
                <a href="/<%: lang %>/Photos/<%: photoFolderName %>/Photo?id=<%: prevPhotoName %>" class="<%: ((prevPhotoName == null) ? "disabled" : "button") %>">Пред.</a>
            </td>
            <td style="width: 700px">
                <img src="<%: photoSource %>" alt="" style="width: 700px" />
            </td>
            <td style="width: 50px">
                <a href="/<%: lang %>/Photos/<%: photoFolderName %>/Photo?id=<%: nextPhotoName %>" class="<%: ((nextPhotoName == null) ? "disabled" : "button") %>">След.</a>
            </td>
            <td>
                <table border="1" style="width: 100%">
                    <tr>
                        <td style="width: 160px">Производитель камеры
                        </td>
                        <td>
                            <%: cameraManufacturer %>
                        </td>
                    </tr>
                    <tr>
                        <td>Модель камеры
                        </td>
                        <td>
                            <%: cameraModel %>
                        </td>
                    </tr>
                    <tr>
                        <td>ПО камеры
                        </td>
                        <td>
                            <%: cameraSoftware %>
                        </td>
                    </tr>
                    <tr>
                        <td>Время и дата съёмки
                        </td>
                        <td>
                            <%: photoDateTime %>
                        </td>
                    </tr>
                    <tr>
                        <td>Выдержка
                        </td>
                        <td>
                            <%: exposureTime %>
                        </td>
                    </tr>
                    <tr>
                        <td>Чувствительность ISO
                        </td>
                        <td>
                            <%: ISO %>
                        </td>
                    </tr>
                    <tr>
                        <td>Диафрагма
                        </td>
                        <td>
                            <%: fNumber %>
                        </td>
                    </tr>
                    <tr>
                        <td>Фокусное растояние
                        </td>
                        <td>
                            <%: focalLength %>
                        </td>
                    </tr>
                    <%
                        if (IsGPSData)
                        {
                    %>
                    <tr>
                        <td>Место съёмки
                        </td>
                        <td>
                            <a href="https://www.google.com/maps/place/<%: GPSData_DDMMSS %>" target="_blank"><%: GPSData_DDMMSS.Replace("+", ", ") %></a>
                            <br>
                            <a href="https://www.google.com/maps/place/<%: GPSData_DDMMSS %>" target="_blank"><%: GPSData_DDD %></a>
                        </td>
                    </tr>
                    <% 
                        }
                    %>
                    <tr>
                        <td>Размеры
                        </td>
                        <td>
                            <%: photoWidth %> x <%: photoHeight %>
                        </td>
                    </tr>
                    <tr>
                        <td>Тэги
                        </td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Button_Edit" />
                            <asp:AsyncPostBackTrigger ControlID="Button_Save" />
                            <asp:AsyncPostBackTrigger ControlID="Button_Cancel" />
                        </Triggers>
                        <ContentTemplate>
                            <%
                                if (HttpContext.Current.User.Identity.Name == "Admin")
                                {
                            %>
                            <asp:Table Width="100%" runat="server">
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">
                                        <asp:Table Width="100%" runat="server">
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server">
                                                    <asp:Label ID="Label_PhotoDescription" Text="" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server">
                                                    <asp:TextBox ID="TextBox_PhotoDescription" Text="" Rows="10" Width="100%" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                    <asp:TableCell Width="70px" runat="server">
                                        <asp:Table Width="100%" runat="server">
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server">
                                                    <asp:Button ID="Button_Edit" OnClick="Button_Edit_Click" Text="Edit" runat="server" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server">
                                                    <asp:Button ID="Button_Save" OnClick="Button_Save_Click" Text="Save" runat="server" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow runat="server">
                                                <asp:TableCell runat="server">
                                                    <asp:Button ID="Button_Cancel" OnClick="Button_Cancel_Click" Text="Cancel" runat="server" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <%
                                }
                                else
                                {
                            %>
                            <asp:Label ID="Label_PhotoDescription_" Text="" runat="server"></asp:Label>
                            <%
                                }
                            %>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:PlaceHolder>
            </td>
        </tr>
    </table>
</asp:Content>
