﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilmListWebUserControl.ascx.cs" Inherits="FilmListWebUserControl" %>
<%@ Reference Control="~/FilmInListWebUserControl.ascx" %>

<table style="width: 100%;">
    <tr>
        <td>
            <asp:PlaceHolder ID="placeHolder1" runat="server"></asp:PlaceHolder>
        </td>
        <td style="vertical-align:top;">
            <div style="margin-left:10px;">
                <asp:PlaceHolder ID="placeHolder2" runat="server"></asp:PlaceHolder>
            </div>
        </td>
    </tr>
</table>
<br>
