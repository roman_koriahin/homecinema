﻿using System;
using System.Web.UI;

public partial class Pano : Page
{
    private string panoHtmlName;
    private string photoAlbumName;

    protected string panoSrc;
    protected string panoXmlSrc;

    protected void Page_Load(object sender, EventArgs e)
    {
        panoHtmlName = Request["pano"];
        photoAlbumName = Page.RouteData.Values["album"] as string;

        panoSrc = @"/images/Photos/" + photoAlbumName + @"/Pano/" + panoHtmlName + @"/";
        panoXmlSrc = panoSrc + panoHtmlName + ".xml";

        this.Title = panoHtmlName;
    }
}