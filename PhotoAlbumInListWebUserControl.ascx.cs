﻿using System;
using System.Web.UI;

public partial class PhotoAlbumInListWebUserControl : UserControl
{
    public string photoAlbumName;

    protected string lang;

    protected void Page_Load(object sender, EventArgs e)
    {
        lang = Page.RouteData.Values["lang"] as string;
    }
}