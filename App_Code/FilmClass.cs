﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class FilmClass
{
    protected string filmImgPath;
    protected string filmGenre;
    protected string filmDirector;
    protected List<string> filmActorsList;

    public FilmClass(string filmImgPath, string filmGenre, string filmDirector, List<string> filmActorsList)
    {
        this.filmImgPath = filmImgPath;
        this.filmGenre = filmGenre;
        this.filmDirector = filmDirector;
        this.filmActorsList = filmActorsList;
    }
}