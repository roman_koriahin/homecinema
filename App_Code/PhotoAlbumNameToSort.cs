﻿using System;

public class PhotoAlbumNameToSort : IComparable
{
    public string albumName;

    private string text;
    private string date;
    private string day;
    private string month;
    private string year;

    public PhotoAlbumNameToSort(string inputText)
    {
        albumName = inputText;

        int idx = inputText.LastIndexOf(" ");

        if (idx == -1)
        {
            date = inputText;

            text = null;
        }
        else
        {
            date = new string(inputText.ToCharArray(idx + 1, inputText.Length - idx - 1));

            text = new string(inputText.ToCharArray(0, idx));
        }

        date = ParseDate(date);
    }
    
    private string ParseDate(string oldDate)
    {
        // Если дата определяет промежуток, то
        // флаг true означает сортировать по первому
        // дню в промежутке, а false - по последнему
        bool _flag = true;

        string newDate = null;

        string[] dateArr = date.Split('_');

        switch (dateArr.Length)
        {
            case 1: // YYYY
                {
                    year = dateArr[0];

                    newDate = year;
                }
                break;
            case 2: // MM.YYYY
                {
                    month = dateArr[0];
                    year = dateArr[1];

                    newDate = year + "." + month;
                }
                break;
            case 3: // DD.MM.YYYY or DD-DD.MM.YYYY
                {
                    string[] tmpArr = dateArr[0].Split('-');

                    switch (tmpArr.Length)
                    {
                        case 1: // DD.MM.YYYY
                            {
                                day = tmpArr[0];
                            }
                            break;
                        case 2: // DD-DD.MM.YYYY
                            {
                                if (_flag)
                                {
                                    day = tmpArr[0];
                                }
                                else
                                {
                                    day = tmpArr[1];
                                }
                            }
                            break;
                    }

                    month = dateArr[1];
                    year = dateArr[2];

                    newDate = year + "." + month + "." + day;
                }
                break;
            case 4: // DD.MM-DD.MM.YYYY
                {
                    //28_04-01_05_2018

                    //dateArr[0] = 28;
                    //dateArr[1] = 04-01;
                    //dateArr[2] = 05;
                    //dateArr[3] = 2018;

                    string firstNumber = dateArr[0];

                    string[] tmpArr = dateArr[1].Split('-');
                    string firstMonth = tmpArr[0];
                    string secondNumber = tmpArr[1];

                    string secondMonth = dateArr[2];

                    if (_flag)
                    {
                        day = firstNumber;
                        month = firstMonth;
                    }
                    else
                    {
                        day = secondNumber;
                        month = secondMonth;
                    }
                    year = dateArr[3];

                    newDate = year + "." + month + "." + day;
                }
                break;
            case 5: // DD.MM.YYYY-DD.MM.YYYY
                {
                    //28_12_2018-02_01_2019

                    //dateArr[0] = 28;
                    //dateArr[1] = 12;
                    //dateArr[2] = 2018-02;
                    //dateArr[3] = 01;
                    //dateArr[4] = 2019;

                    string firstNumber = dateArr[0];
                    string firstMonth = dateArr[1];

                    string[] tmpArr = dateArr[2].Split('-');
                    string firstYear = tmpArr[0];
                    string secondNumber = tmpArr[1];

                    string secondMonth = dateArr[3];
                    string secondYear = dateArr[4];

                    if (_flag)
                    {
                        day = firstNumber;
                        month = firstMonth;
                        year = firstYear;
                    }
                    else
                    {
                        day = secondNumber;
                        month = secondMonth;
                        year = secondYear;
                    }

                    newDate = year + "." + month + "." + day;
                }
                break;
        }

        return newDate;
    }

    int IComparable.CompareTo(object obj)
    {
        PhotoAlbumNameToSort otherPhotoAlbumName = obj as PhotoAlbumNameToSort;

        if (otherPhotoAlbumName != null)
        {
            return CompareTo(otherPhotoAlbumName);
        }
        else
        {
            throw new ArgumentException("Object is not a PhotoPholderNameToSort");
        }
    }

    private int CompareTo(PhotoAlbumNameToSort otherPhotoAlbumName)
    {
        int a = date.CompareTo(otherPhotoAlbumName.date);

        if (a > 0)
        {
            return -1;
        }
        else if (a < 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}





//using System;

//public class PhotoAlbumNameToSort : IComparable
//{
//    private long createDate;
//    public string albumName;

//    public PhotoAlbumNameToSort(long createDate, string albumName)
//    {
//        this.createDate = createDate;
//        this.albumName = albumName;
//    }

//    int IComparable.CompareTo(object obj)
//    {
//        PhotoAlbumNameToSort otherPhotoAlbumName = obj as PhotoAlbumNameToSort;

//        if (otherPhotoAlbumName != null)
//        {
//            return CompareTo(otherPhotoAlbumName);
//        }
//        else
//        {
//            throw new ArgumentException("Object is not a PhotoAlbumNameToSort");
//        }
//    }

//    private int CompareTo(PhotoAlbumNameToSort otherPhotoAlbumName)
//    {
//        int a = createDate.CompareTo(otherPhotoAlbumName.createDate);

//        if (a > 0)
//        {
//            return -1;
//        }
//        else if (a < 0)
//        {
//            return 1;
//        }
//        else
//        {
//            return 0;
//        }
//    }
//}
