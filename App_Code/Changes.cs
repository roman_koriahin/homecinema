﻿public enum Changes
{
    None,
    Save,
    Update,
    Delete
}