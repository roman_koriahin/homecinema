﻿using System;
using System.Web.UI;

public partial class ErrorMainPage : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string query = Request.Url.Query;
        int idx = query.LastIndexOf('?');
        string error = "";

        if (idx > -1)
        {
            error = new string(query.ToCharArray(), idx + 1, 3);
        }

        switch (error)
        {
            case "403":
                {
                    error = error + " Forbidden";
                    Response.Status = error;
                    error = "Error " + error + "!";
                }
                break;
            case "404":
                {
                    error = error + " Not Found";
                    Response.Status = error;
                    error = "Error " + error + "!";
                }
                break;
            case "502":
                {
                    error = error + " Bad Gateway";
                    Response.Status = error;
                    error = "Error " + error + "!";
                }
                break;
            default:
                {
                    error = "Неизвестная ошибка!";
                }
                break;
        }
        
        errorStatus.InnerText = error;

        requestedURL.InnerText = Request["aspxerrorpath"] ?? Request.RawUrl;
        errorSrc.InnerText = Request["aspxerrorpath"] == null ? "IIS" : "ASP.NET";
    }
}