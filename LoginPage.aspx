﻿<%@ Page Title="Страница авторизации" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LoginPage.aspx.cs" Inherits="LoginPage" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <h2>Пожалуйста, войдите в систему</h2>

    <div class="col-md-8">
        <div class="form-horizontal">
            <hr>
            <table style="width: 100%;" border="0">
                <tr>
                    <td style="height: 43px; width: 23%; vertical-align: top;">
                        <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label" Style="width:100%; text-align:left;">Имя пользователя</asp:Label>
                    </td>
                    <td>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="UserName" Width="80%" CssClass="form-control" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="UserName" ForeColor="Red" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName" ErrorMessage="Некорректное имя пользователя" ValidationExpression="[\w| ]*" ForeColor="Red" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 43px; width: 23%; vertical-align: top;">
                        <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label" Style="width:100%; text-align:left;">Пароль</asp:Label>
                    </td>
                    <td>
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" Width="80%" CssClass="form-control" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="Password" ForeColor="Red" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Password" ErrorMessage="Некорректный пароль" ValidationExpression='[\w| !"§$%&amp;/()=\-?\*]*' ForeColor="Red" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="height: 43px; text-align: left;">
                        <div class="col-md-10">
                            <asp:Button runat="server" OnClick="LoginAction_Click" Text="Вход" CssClass="btn btn-default" />
                            <br>
                            <asp:Label ID="LegendStatus" runat="server" EnableViewState="false" Text="" ForeColor="Red" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br>
    <br>
    <br>

</asp:Content>
