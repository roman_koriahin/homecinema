﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;

    protected string lang;

    protected void Page_Init(object sender, EventArgs e)
    {
        // Код ниже защищает от XSRF-атак
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;
        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            // Использование маркера защиты от XSRF из файла cookie
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            // Создание нового маркера защиты от XSRF и его сохранение в файле cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
            {
                responseCookie.Secure = true;
            }
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        lang = Page.RouteData.Values["lang"] as string;

        if (lang != null)
        {
            SearchTextBox.Text = lang;

            switch (lang)
            {
                case "ua":
                    {
                        ImageButton_LanguageUA.Width = 40;
                        ImageButton_LanguageUA.Height = 30;

                        ImageButton_LanguageRU.Width = 32;
                        ImageButton_LanguageRU.Height = 24;

                        ImageButton_LanguageEN.Width = 32;
                        ImageButton_LanguageEN.Height = 24;

                        LogoutLinkButton.Text = "Вихід";
                    }
                    break;
                case "ru":
                    {
                        ImageButton_LanguageUA.Width = 32;
                        ImageButton_LanguageUA.Height = 24;

                        ImageButton_LanguageRU.Width = 40;
                        ImageButton_LanguageRU.Height = 30;

                        ImageButton_LanguageEN.Width = 32;
                        ImageButton_LanguageEN.Height = 24;

                        LogoutLinkButton.Text = "Выход";
                    }
                    break;
                case "en":
                    {
                        ImageButton_LanguageUA.Width = 32;
                        ImageButton_LanguageUA.Height = 24;

                        ImageButton_LanguageRU.Width = 32;
                        ImageButton_LanguageRU.Height = 24;

                        ImageButton_LanguageEN.Width = 40;
                        ImageButton_LanguageEN.Height = 30;

                        LogoutLinkButton.Text = "Logout";
                    }
                    break;
                default:
                    {
                        lang = "ru";

                        Response.Redirect("/" + lang + Request.RawUrl);
                    }
                    break;
            }
        }
        else
        {
            lang = "ru";

            Response.Redirect("/" + lang + Request.RawUrl);
        }
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Задание маркера защиты от XSRF
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            // Проверка маркера защиты от XSRF
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Ошибка проверки маркера защиты от XSRF.");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        GC.Collect();
    }

    protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Context.GetOwinContext().Authentication.SignOut();
    }

    protected void LogoutLinkButton_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();

        Response.Redirect(Request.RawUrl);
    }

    protected void SearchLinkButton_Click(object sender, EventArgs e)
    {

    }

    protected void ImageButton_LanguageUA_Click(object sender, ImageClickEventArgs e)
    {
        string urlString = Request.Url.AbsoluteUri;
        urlString = urlString.Replace("/ru", "/ua");
        urlString = urlString.Replace("/en", "/ua");
        urlString = urlString.Replace("lang=ru", "lang=ua");
        urlString = urlString.Replace("lang=en", "lang=ua");

        Response.Redirect(urlString);
    }

    protected void ImageButton_LanguageRU_Click(object sender, ImageClickEventArgs e)
    {
        string urlString = Request.Url.AbsoluteUri;
        urlString = urlString.Replace("/ua", "/ru");
        urlString = urlString.Replace("/en", "/ru");
        urlString = urlString.Replace("lang=ua", "lang=ru");
        urlString = urlString.Replace("lang=en", "lang=ru");

        Response.Redirect(urlString);
    }

    protected void ImageButton_LanguageEN_Click(object sender, ImageClickEventArgs e)
    {
        string urlString = Request.Url.AbsoluteUri;
        urlString = urlString.Replace("/ru", "/en");
        urlString = urlString.Replace("/ua", "/en");
        urlString = urlString.Replace("lang=ru", "lang=en");
        urlString = urlString.Replace("lang=ua", "lang=en");

        Response.Redirect(urlString);
    }
}