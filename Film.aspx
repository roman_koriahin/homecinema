﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Film.aspx.cs" Inherits="Film" %>

<%@ Register TagPrefix="filmInfoWebUserControl" TagName="FilmInfoWebUserControl" Src="~/FilmInfoWebUserControl.ascx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 style="text-align: center;"><%: filmNameRus %></h1>
    <%
        if (Request.Browser.IsMobileDevice)
        {
    %>
    <table border="1">
        <tr>
            <td style="vertical-align: top;">
                <div style="width: 50%; margin: auto;">
                    <img src="/<%: imgPath %>" alt="" style="width: 100%; overflow: hidden;" />
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table border="1" style="width: 100%">
                    <tr>
                        <td style="width: 150px">
                            <div style="margin-left: 2px;">Название на английском</div>
                        </td>
                        <td>
                            <div style="margin-left: 2px;"><%: filmNameEng %></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Язык оригинала</div>
                        </td>
                        <td>
                            <a href="/<%: lang %>/Films?filter=langoriginal&value=<%: filmLangOriginal %>" class="linkbutton"><%: filmLangOriginal %></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Язык дубляжа</div>
                        </td>
                        <td>
                            <a href="/<%: lang %>/Films?filter=langdub&value=<%: filmLangDub %>" class="linkbutton"><%: filmLangDub %></a>
                        </td>
                    </tr>
                    <%
                        if (filmLangDub == "Украинский")
                        {
                    %>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Название на украинском</div>
                        </td>
                        <td>
                            <div style="margin-left: 2px;"><%: filmNameUkr %></div>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Год</div>
                        </td>
                        <td>
                            <%
                                if (filmYear != null && filmYear != "")
                                {
                            %>
                            <a href="/<%: lang %>/Films?filter=year&value=<%: filmYear %>" class="linkbutton"><%: filmYear %></a>
                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Режисёр</div>
                        </td>
                        <td>
                            <asp:PlaceHolder runat="server" ID="placeHolderFilmDirector_mob"></asp:PlaceHolder>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Жанр</div>
                        </td>
                        <td>
                            <asp:PlaceHolder runat="server" ID="placeHolderFilmGenre_mob"></asp:PlaceHolder>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Продолжительность</div>
                        </td>
                        <td>
                            <%
                                if (filmRunningTime != null && filmRunningTime != "" && filmRunningTime != "NULL")
                                {
                            %>
                            <div style="margin-left: 2px;"><%: filmRunningTime %> мин.</div>
                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Актёры</div>
                        </td>
                        <td>
                            <asp:PlaceHolder runat="server" ID="placeHolderFilmActors_mob"></asp:PlaceHolder>
                        </td>
                    </tr>
                    <%
                        if (prevFilmName != null && prevFilmName != "")
                        {
                    %>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Предыдущий фильм</div>
                        </td>
                        <td>
                            <div style="margin-left: 2px;"><a href="<%: prevFilmId %>"><%: prevFilmName %></a></div>
                        </td>
                    </tr>
                    <%
                        }

                        if (nextFilmName != null && nextFilmName != "")
                        {
                    %>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Следующий фильм</div>
                        </td>
                        <td>
                            <div style="margin-left: 2px;"><a href="<%: nextFilmId %>"><%: nextFilmName %></a></div>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="margin-left: 2px;">
                    <asp:Label ID="Label_FilmDescription1" Text="" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
        <%
            if (HttpContext.Current.User.Identity.Name == "Admin")
            {
        %>
        <tr>
            <td colspan="2">
                <div style="margin-left: 2px;">
                    <a href="">Редактировать</a>
                </div>
            </td>
        </tr>
        <%
            }
        %>
        <tr>
            <td colspan="2">
                <video width="100%" controls="">
                    <source src="<%: filmSource %>" type="video/mp4">
                </video>
            </td>
        </tr>
    </table>
    <%
        }
        else
        {
    %>
    <table border="1" style="width: 80%; margin: auto;">
        <tr>
            <td style="width: 300px; vertical-align: top;">
                <div style="width: 100%; margin: auto;">
                    <img src="/<%: imgPath %>" alt="" style="width: 100%; overflow: hidden;" />
                </div>
            </td>
            <td style="vertical-align: top;">
                <table border="1" style="width: 100%">
                    <tr>
                        <td style="width: 170px">
                            <div style="margin-left: 2px;">Название на английском</div>
                        </td>
                        <td>
                            <div style="margin-left: 2px;"><%: filmNameEng %></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Язык оригинала</div>
                        </td>
                        <td>
                            <a href="/<%: lang %>/Films?filter=langoriginal&value=<%: filmLangOriginal %>" class="linkbutton"><%: filmLangOriginal %></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Язык дубляжа</div>
                        </td>
                        <td>
                            <a href="/<%: lang %>/Films?filter=langdub&value=<%: filmLangDub %>" class="linkbutton"><%: filmLangDub %></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Год</div>
                        </td>
                        <td>
                            <a href="/<%: lang %>/Films?filter=year&value=<%: filmYear %>" class="linkbutton"><%: filmYear %></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Режисёр</div>
                        </td>
                        <td>
                            <asp:PlaceHolder runat="server" ID="placeHolderFilmDirector_full"></asp:PlaceHolder>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Жанр</div>
                        </td>
                        <td>
                            <asp:PlaceHolder runat="server" ID="placeHolderFilmGenre_full"></asp:PlaceHolder>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Продолжительность</div>
                        </td>
                        <td>
                        <%
                            if (filmRunningTime != null && filmRunningTime != "" && filmRunningTime != "NULL")
                            {
                                %>
                                <div style="margin-left: 2px;"><%: filmRunningTime %> мин.</div>
                                <%
                            }
                        %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-left: 2px;">Актёры</div>
                        </td>
                        <td>
                            <asp:PlaceHolder runat="server" ID="placeHolderFilmActors_full"></asp:PlaceHolder>
                        </td>
                    </tr>
                    <%
                        if (prevFilmName != null && prevFilmName != "")
                        {
                            %>
                            <tr>
                                <td>
                                    <div style="margin-left: 2px;">Предыдущий фильм</div>
                                </td>
                                <td>
                                    <div style="margin-left: 2px;">
                                        <a href="<%: prevFilmId %>"><%: prevFilmName %></a>
                                    </div>
                                </td>
                            </tr>
                            <%
                        }

                        if (nextFilmName != null && nextFilmName != "")
                        {
                            %>
                            <tr>
                                <td>
                                    <div style="margin-left: 2px;">Следующий фильм</div>
                                </td>
                                <td>
                                    <div style="margin-left: 2px;">
                                        <a href="<%: nextFilmId %>"><%: nextFilmName %></a>
                                    </div>
                                </td>
                            </tr>
                            <%
                        }
                    %>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="margin-left: 2px;">
                    <asp:UpdatePanel ID="UpdatePanel_FilmDescription2" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Button_Edit" />
                            <asp:AsyncPostBackTrigger ControlID="Button_Cancel" />
                            <asp:AsyncPostBackTrigger ControlID="Button_Save" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Label runat="server" ID="Label_FilmDescription2" Text=""></asp:Label>
                            <asp:TextBox runat="server" ID="TextBox_FilmDescription2" Text="" Visible="false" TextMode="MultiLine" Rows="6" Width="100%"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
        <%
            if (HttpContext.Current.User.Identity.Name == "Admin")
            {
        %>
        <tr>
            <td colspan="2">
                <div style="margin-left: 2px;">
                    <asp:Button ID="Button_Edit" Text="Редактировать" OnClick="Button_Edit_Click" runat="server" CssClass="btn btn-default" />
                    <asp:Button ID="Button_Save" Text="Сохранить" OnClick="Button_Save_Click" runat="server" CssClass="btn btn-default" />
                    <asp:Button ID="Button_Cancel" Text="Отмена" OnClick="Button_Cancel_Click" runat="server" CssClass="btn btn-default" />
                </div>
            </td>
        </tr>
        <%
            }
        %>
        <tr>
            <td colspan="2" style="text-align: center;">
                <video controls="" style="height: 300px;">
                    <source src="<%: filmSource %>" type="video/mp4">
                </video>
            </td>
        </tr>
    </table>
    <%
        }
    %>
    <br>
</asp:Content>
