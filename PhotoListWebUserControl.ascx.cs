﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PhotoListWebUserControl : UserControl
{
    public int pageNumber;
    public string photoAlbumName;

    private int photosOnePageMult;
    private int photosOnePageHeight = 10;
    private int photosOnePage;
    private int photosCount;
    private int pageCount;

    protected string lang = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        lang = Page.RouteData.Values["lang"] as string;

        photosOnePageMult = (Request.Browser.IsMobileDevice) ? 1 : 3;

        photosOnePage = photosOnePageHeight * photosOnePageMult;

        Table table = new Table();
        table.CellPadding = 4;
        table.GridLines = GridLines.None;

        if (pageNumber > 0)
        {
            ReadPhotosFromHDD();

            if (pageCount > 1)
            {
                TableRow tableRowPrev = new TableRow();
                TableCell tableCellPrev = new TableCell();
                tableCellPrev.Text = "<a href=\"/" + lang + "/Photos/" + photoAlbumName + "?page=" + (pageNumber - 1) + "\" class=\"" + ((pageNumber == 1) ? "disabled" : "button") + "\">Пред.</a>";
                tableRowPrev.Cells.Add(tableCellPrev);
                table.Rows.Add(tableRowPrev);

                for (int i = 0; i < pageCount; i++)
                {
                    TableRow tableRow = new TableRow();
                    TableCell tableCell = new TableCell();
                    tableCell.Text = "<a href=\"/" + lang + "/Photos/" + photoAlbumName + "?page=" + (i + 1) + "\" class=\"button\">" + (i + 1) + "</a>";
                    tableRow.Cells.Add(tableCell);
                    table.Rows.Add(tableRow);
                }

                TableRow tableRowNext = new TableRow();
                TableCell tableCellNext = new TableCell();
                tableCellNext.Text = "<a href=\"/" + lang + "/Photos/" + photoAlbumName + "/" + (pageNumber + 1) + "/\" class=\"" + ((pageNumber == pageCount) ? "disabled" : "button") + "\">След.</a>";
                tableRowNext.Cells.Add(tableCellNext);
                table.Rows.Add(tableRowNext);
            }
        }

        placeHolder1.Controls.Add(table);
    }

    private void ReadPhotosFromHDD()
    {
        string photoFolderPath = @"D:/inetpub/wwwroot/HomeCinema/Images/Photos/" + photoAlbumName;

        string[] photos = Directory.GetFiles(photoFolderPath, "*.jpg").Select(x => Path.GetFileNameWithoutExtension(x)).ToArray();

        photosCount = photos.Length;

        int start = 0;
        int end = 0;

        CalculateStartEnd(out start, out end);

        Table table = new Table();

        for (int i = 0; i < photosOnePageHeight; i++)
        {
            TableRow tableRow = new TableRow();

            for (int j = 0; j < photosOnePageMult; j++)
            {
                int photoIdx = start + i * photosOnePageMult + j;

                if (photoIdx <= end)
                {
                    string photoName = photos[photoIdx];

                    TableCell tableCell = new TableCell();

                    if (Request.Browser.IsMobileDevice)
                    {
                        tableCell.Text = "<div style=\"width: 250px; height: 167px; overflow: hidden; margin: 1px;\"><a href=\"/" + lang + "/Photos/" + photoAlbumName + "/Photo?id=" + photoName + "\" target=\"_blank\"><img src=\"/Images/Photos/" + photoAlbumName + "/Preview/" + photoName + ".jpg\" alt=\"\" style=\"width:100%;overflow:hidden;\"/></a><div>";
                    }
                    else
                    {
                        tableCell.Text = "<div style=\"width: 300px; height: 200px; overflow: hidden; margin: 5px;\"><a href=\"/" + lang + "/Photos/" + photoAlbumName + "/Photo?id=" + photoName + "\" target=\"_blank\"><img src=\"/Images/Photos/" + photoAlbumName + "/Preview/" + photoName + ".jpg\" alt=\"\" style=\"width:100%;overflow:hidden;\"/></a><div>";
                    }

                    tableRow.Cells.Add(tableCell);
                }
            }
            
            table.Rows.Add(tableRow);
        }

        placeHolder2.Controls.Add(table);

        if (pageNumber == pageCount)
        {
            if (Directory.Exists(photoFolderPath + @"\Pano"))
            {
                string[] panoDirs = Directory.GetDirectories(photoFolderPath + @"\Pano").Select(x => Path.GetFileNameWithoutExtension(x)).ToArray();

                foreach (var pano in panoDirs)
                {
                    string panoXmlSrc = photoFolderPath + @"\Pano\" + pano + @"\" + pano + ".xml";

                    if (File.Exists(panoXmlSrc))
                    {
                        PanoInPhotoListWebUserControl panoInPhotoListWebUserControl = (PanoInPhotoListWebUserControl)LoadControl("~/PanoInPhotoListWebUserControl.ascx");
                        panoInPhotoListWebUserControl.photoAlbumName = photoAlbumName;
                        panoInPhotoListWebUserControl.panoHtmlName = pano;
                        placeHolder2.Controls.Add(panoInPhotoListWebUserControl);
                    }
                }
            }

            string[] videos = Directory.GetFiles(photoFolderPath, "*.mp4").Select(x => Path.GetFileNameWithoutExtension(x)).ToArray();

            if (videos.Length > 0)
            {
                foreach (var video in videos)
                {
                    VideoInPhotoListWebUserControl videoInPhotoListWebUserControl = (VideoInPhotoListWebUserControl)LoadControl("~/VideoInPhotoListWebUserControl.ascx");
                    videoInPhotoListWebUserControl.photoAlbumName = photoAlbumName;
                    videoInPhotoListWebUserControl.videoName = video;
                    placeHolder2.Controls.Add(videoInPhotoListWebUserControl);
                }
            }
        }
    }

    private void CalculateStartEnd(out int start, out int end)
    {
        if (photosCount % photosOnePage != 0)
        {
            pageCount = ((photosCount / photosOnePage) * photosOnePage + photosOnePage) / photosOnePage;
        }
        else
        {
            pageCount = photosCount / photosOnePage;
        }

        start = pageNumber * photosOnePage - photosOnePage;
        end = pageNumber * photosOnePage - 1;

        if (end >= photosCount)
        {
            end = photosCount - 1;
        }
    }
}