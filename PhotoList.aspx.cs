﻿using System;
using System.Web.UI;

public partial class PhotoList : Page
{
    protected string photoAlbumName;

    protected void Page_Load(object sender, EventArgs e)
    {
        int pageNumber = 1;

        string lang = Page.RouteData.Values["lang"] as string;
        string page = Request.QueryString["page"];

        if (page != null)
        {
            pageNumber = Convert.ToInt32(page);
        }

        photoAlbumName = Page.RouteData.Values["album"] as string;

        this.Title = photoAlbumName.Replace("_", ".") + ". Страница " + pageNumber;

        PhotoListWebUserControl photoListWebUserControl = (PhotoListWebUserControl)LoadControl("~/PhotoListWebUserControl.ascx");
        photoListWebUserControl.pageNumber = pageNumber;
        photoListWebUserControl.photoAlbumName = photoAlbumName;
        placeHolder1.Controls.Add(photoListWebUserControl);
    }
}