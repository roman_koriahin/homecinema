﻿<%@ Application Language="C#" %>
<%@ Import Namespace="HomeCinema" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
        BundleConfig.RegisterBundles(BundleTable.Bundles);

        RegisterCustomRoutes(RouteTable.Routes);
    }

    void RegisterCustomRoutes(RouteCollection routes)
    {
        routes.MapPageRoute(
            "AdminMainPage",
            "{lang}/Admin/",
            "~/Admin/AdminMainPage.aspx"
        );

        routes.MapPageRoute(
            "AdminFilmsList",
            "{lang}/Admin/FilmList/",
            "~/Admin/FilmsList.aspx"
        );

        routes.MapPageRoute(
            "HomePage_v1",
            "{lang}",
            "~/Default.aspx"
        );

        routes.MapPageRoute(
            "HomePage_v2",
            "{lang}/Home/",
            "~/Default.aspx"
        );

        routes.MapPageRoute(
            "Film",
            "{lang}/Film/{id}/",
            "~/Film.aspx"
        );

        routes.MapPageRoute(
            "FilmList",
            "{lang}/Films/",
            "~/FilmList.aspx"
        );

        routes.MapPageRoute(
            "PhotoAlbumList",
            "{lang}/Photos/",
            "~/PhotoAlbumList.aspx"
        );

        routes.MapPageRoute(
            "PhotoList",
            "{lang}/Photos/{album}/",
            "~/PhotoList.aspx"
        );

        routes.MapPageRoute(
            "Photo",
            "{lang}/Photos/{album}/Photo",
            "~/Photo.aspx"
        );

        routes.MapPageRoute(
            "Pano",
            "{lang}/Photos/{album}/Pano",
            "~/Pano.aspx"
        );

        routes.MapPageRoute(
            "Video",
            "{lang}/Photos/{album}/Video",
            "~/Video.aspx"
        );

        routes.MapPageRoute(
            "LoginPage",
            "{lang}/LoginPage/",
            "~/LoginPage.aspx"
        );
    }

</script>
