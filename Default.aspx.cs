﻿using System;
using System.Web.UI;

public partial class _Default : Page
{
    protected string lang;

    protected void Page_Load(object sender, EventArgs e)
    {
        lang = Page.RouteData.Values["lang"] as string;

        if (lang == null)
        {
            lang = Request["lang"];
        }

        if (lang != null)
        {
            switch (lang)
            {
                case "ua":
                    {
                        this.Title = "Головна сторінка";
                    }
                    break;
                case "ru":
                    {
                        this.Title = "Главная страница";
                    }
                    break;
                case "en":
                    {
                        this.Title = "Main page";
                    }
                    break;
            }
        }
    }
}