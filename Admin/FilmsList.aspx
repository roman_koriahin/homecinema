﻿<%@ Page Title="Изменение библиотеки фильмов" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="FilmsList.aspx.cs" Inherits="ChangeFilmsLib" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <br>
    <asp:PlaceHolder ID="PlaceHolderTable" runat="server">
        <asp:UpdatePanel ID="MainUpdatePanel" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button_Save" />
                <asp:AsyncPostBackTrigger ControlID="Button_Cancel" />
                <asp:AsyncPostBackTrigger ControlID="Button_DeleteFilm" />
                <asp:AsyncPostBackTrigger ControlID="Button_DeleteFilm_Yes" />
                <asp:AsyncPostBackTrigger ControlID="Button_DeleteFilm_No" />
            </Triggers>
            <ContentTemplate>
                <asp:Table ID="MainTable" runat="server" Width="100%">
                    <asp:TableRow runat="server">
                        <asp:TableCell ID="MainTableCell_FilmList" runat="server" Width="25%" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" VerticalAlign="Top">
                            <asp:Panel ID="Panel_FilmList" runat="server" Style="width: 100%; height: calc(100vh - 140px); overflow: auto;" BorderColor="Black" BorderWidth="1px" BorderStyle="Solid">
                                <asp:Table ID="Table_FilmList" runat="server" Width="100%">
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell ID="MainTableCell_Actions" runat="server" Width="75%" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" VerticalAlign="Top">
                            <asp:Table ID="Table_Actions" Width="100%" runat="server">
                                <asp:TableRow ID="TableRow_ChangeFilmInfo" runat="server">
                                    <asp:TableCell ID="TableCell_ChangeFilmInfo" ColumnSpan="3" runat="server">
                                        <div class="form-horizontal">
                                            <asp:Table ID="Table_ChangeFilmInfo" Width="100%" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                <asp:TableRow runat="server">
                                                    <asp:TableCell ColumnSpan="3" runat="server">
                                                        <asp:Label ID="Label_Message" Text="" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server">
                                                    <asp:TableCell ColumnSpan="3" runat="server">
                                                        <asp:Label ID="Label_Error" Text="" ForeColor="Red" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmId" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmId" Text="ID фильма (FilmId)" AssociatedControlID="TextBox_FilmID" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmId" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmID" Enabled="false" Style="width: 100%;" Text="FilmId" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmFileName" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmFileName" Text="Имя .mp4 файла (FilmFileName)" AssociatedControlID="TextBox_FilmFileName" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmFileName" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmFileName" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmNameEng" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmNameEng" Text="Название на английском (FilmName)" AssociatedControlID="TextBox_FilmNameEng" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmNameEng" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmNameEng" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmLangOriginal" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmLangOriginal" Text="Язык оригинала (FilmLangOriginal)" AssociatedControlID="TextBox_FilmLangOriginal" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmLangOriginal" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmLangOriginal" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmLangDub" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmLangDub" Text="Язык дубляжа (FilmLangDub)" AssociatedControlID="TextBox_FilmLangDub" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmLangDub" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmLangDub" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmNameRus" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmNameRus" Text="Название на русском (FilmNameRus)" AssociatedControlID="TextBox_FilmNameRus" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmNameRus" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmNameRus" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmNameUkr" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmNameUkr" Text="Название на украинском (FilmNameUkr)" AssociatedControlID="TextBox_FilmNameUkr" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmNameUkr" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmNameUkr" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmYear" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmYear" Text="Год (FilmYear)" AssociatedControlID="TextBox_FilmYear" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmYear" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmYear" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmDirector" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmDirector" Text="Режиссёр (FilmDirector)" AssociatedControlID="TextBox_FilmDirector" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmDirector" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmDirector" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmGenre" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmGenre" Text="Жанр (FilmGenre)" AssociatedControlID="TextBox_FilmGenre" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmGenre" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmGenre" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmRunningTime" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmRunningTime" Text="Длительность (FilmRunningTime)" AssociatedControlID="TextBox_FilmRunningTime" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmRunningTime" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmRunningTime" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_FilmActors" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_FilmActors" Text="Актёры (FilmActors)" AssociatedControlID="TextBox_FilmActors" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_FilmActors" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_FilmActors" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_PrevFilmId" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_PrevFilmId" Text="ID предыдущего фильма (PrevFilmId)" AssociatedControlID="TextBox_PrevFilmId" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_PrevFilmId" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_PrevFilmId" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Label_NextFilmId" Style="width: 33%;" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="Label_NextFilmId" Text="ID следующего фильма (NextFilmId)" AssociatedControlID="TextBox_NextFilmId" CssClass="col-md-2 control-label" Style="width: 100%; text-align: left;" runat="server"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_TextBox_NextFilmId" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:TextBox ID="TextBox_NextFilmId" Style="width: 100%;" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow runat="server" Style="height: 43px; vertical-align: top;">
                                                    <asp:TableCell ID="TableCell_Button_Save" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:Button ID="Button_Save" Text="Сохранить" OnClick="Button_Save_Click" runat="server" CssClass="btn btn-default" />
                                                        </div>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_Button_Cancel" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:Button ID="Button_Cancel" Text="Отмена" OnClick="Button_Cancel_Click" runat="server" CssClass="btn btn-default" />
                                                        </div>
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="TableCell_Button_DeleteFilm" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                        <div class="col-md-10">
                                                            <asp:Button ID="Button_DeleteFilm" Text="Удалить" OnClick="Button_DeleteFilm_Click" runat="server" CssClass="btn btn-default" />
                                                        </div>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </div>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="TableRow_Confirmations" runat="server">
                                    <asp:TableCell ID="TableCell_DeleteFilm" runat="server" HorizontalAlign="Center">
                                        <asp:Table ID="Table_DeleteFilm" Width="250px" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                            <asp:TableRow ID="TableRow_DeleteFilm_1" runat="server">
                                                <asp:TableCell ID="TableCell_DeleteFilm_Label" ColumnSpan="2" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                    <asp:Label ID="Label_FilmName_DeleteFilm" Text="Label_FilmName_DeleteFilm" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow ID="TableRow_DeleteFilm_2" runat="server">
                                                <asp:TableCell ID="TableCell_DeleteFilm_Button_Yes" runat="server" HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                    <asp:Button ID="Button_DeleteFilm_Yes" Width="100px" Text="Да" OnClick="Button_Delete_Yes_Click" runat="server" CssClass="btn btn-default" />
                                                </asp:TableCell>
                                                <asp:TableCell ID="TableCell_DeleteFilm_Button_No" runat="server" HorizontalAlign="Center" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                                    <asp:Button ID="Button_DeleteFilm_No" Width="100px" Text="Нет" OnClick="Button_Delete_No_Click" runat="server" CssClass="btn btn-default" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:PlaceHolder>
    <p></p>
    <p></p>
</asp:Content>
