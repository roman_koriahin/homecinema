﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="AdminMainPage.aspx.cs" Inherits="AdminMainPage" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %></h2>
    <p><a href="FilmList/"><%: filmsListTXT %></a></p>
    <p><a href="FileUploader/"><%: fileUploaderTXT %></a></p>
    <p><a href="ServerMonitor/"><%: serverMonitorTXT %></a></p>
    <p><a href="ServerLog/"><%: serverLogTXT %></a></p>
    <p></p>
</asp:Content>
