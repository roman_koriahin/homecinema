﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ChangeFilmsLib : Page
{
    bool _flag = false;

    List<string> filmsHDD;
    Dictionary<int, string[]> filmsDB;

    protected void Page_Load(object sender, EventArgs e)
    {
        ReadFilmList();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (MyStaticClass.changes != Changes.None && IsPostBack && _flag)
        {
            UpdateFilmList();
        }

        if (!IsPostBack)
        {
            HideTables();
        }
    }

    private void UpdateFilmList()
    {
        Table_FilmList.Rows.Clear();

        ReadFilmList();

        _flag = false;
    }

    private void ReadFilmList()
    {
        filmsDB = new Dictionary<int, string[]>();

        string[] films = Directory.GetFiles(@"D:\inetpub\wwwroot\HomeCinema\Videos", "*.mp4").Select(x => Path.GetFileName(x)).ToArray();

        filmsHDD = new List<string>(films);

        using (SqlConnection cn = new SqlConnection())
        {
            cn.ConnectionString = @"Data Source=localhost\MSSQLEXPRESS;Initial Catalog=FilmsDB;Integrated Security=True;Pooling=False";
            cn.Open();

            string strSQL = "SELECT * FROM FilmTable ORDER BY FilmId";
            SqlCommand myCommand = new SqlCommand(strSQL, cn);
            SqlDataReader dr = myCommand.ExecuteReader();

            while (dr.Read())
            {
                filmsDB.Add(Convert.ToInt32(dr["FilmId"]), new string[] { dr["FilmNameRus"].ToString(), dr["FilmFileName"].ToString() });
            }

            cn.Close();
        }

        foreach (var item in filmsDB)
        {
            AsyncPostBackTrigger asyncPostBackTrigger = new AsyncPostBackTrigger();
            TableRow tableRow = new TableRow();
            TableCell tableCell = new TableCell();
            Button button = new Button
            {
                Text = item.Value[0],
                ID = item.Value[1],
                CssClass = "btn btn-default",
                Width = new Unit("268px")
            };

            if (!filmsHDD.Contains(item.Value[1]))
            {
                button.BackColor = Color.LightPink;
            }
            else
            {
                button.BackColor = Color.FromArgb(170, 255, 255);
            }

            button.Click += Button_Click;
            tableCell.Controls.Add(button);
            tableRow.Cells.Add(tableCell);
            Table_FilmList.Rows.Add(tableRow);
            asyncPostBackTrigger.ControlID = button.ID;
            MainUpdatePanel.Triggers.Add(asyncPostBackTrigger);
        }

        foreach (var item in filmsHDD)
        {
            if (!FilmsDBContainsValue(item))
            {
                AsyncPostBackTrigger asyncPostBackTrigger = new AsyncPostBackTrigger();
                TableRow tableRow = new TableRow();
                TableCell tableCell = new TableCell();
                Button button = new Button
                {
                    Text = item,
                    ID = item,
                    CssClass = "btn btn-default",
                    Width = new Unit("268px"),
                    BackColor = Color.LightGreen
                };
                button.Click += Button_Click;
                tableCell.Controls.Add(button);
                tableRow.Cells.Add(tableCell);
                Table_FilmList.Rows.Add(tableRow);
                asyncPostBackTrigger.ControlID = button.ID;
                MainUpdatePanel.Triggers.Add(asyncPostBackTrigger);
            }
        }
    }

    private bool FilmsDBContainsValue(string filmFileName)
    {
        foreach (var item in filmsDB)
        {
            if (item.Value[1] == filmFileName)
            {
                return true;
            }
        }

        return false;
    }

    private void Button_Click(object sender, EventArgs e)
    {
        MyStaticClass.changes = Changes.None;
        Table_DeleteFilm.Visible = false;

        Button button = (Button)sender;

        Table_ChangeFilmInfo.Visible = true;

        TextBox_FilmID.Text = "";
        TextBox_FilmFileName.Text = MyStaticClass.selectedFilmName = button.ID;

        string strSQL = "";

        if (button.BackColor == Color.FromArgb(170, 255, 255))
        {
            Label_Message.Text = "Внесение изменений в существующую запись в базе данных.";
            strSQL = "SELECT * FROM FilmTable WHERE FilmFileName = '" + MyStaticClass.selectedFilmName + "'";
        }
        else if (button.BackColor == Color.LightPink)
        {
            Label_Message.Text = "Запись о фильме есть в базе данных, но его нет на диске. Предполагается удаление записи о фильме с базы данных.";
            strSQL = "SELECT * FROM FilmTable WHERE FilmFileName = '" + MyStaticClass.selectedFilmName + "'";
        }
        else if (button.BackColor == Color.LightGreen)
        {
            Label_Message.Text = "Фильм есть на диске, но записи о нём нет в базе данных. Предполагается добавление записи о фильме в базу данных.";
            TextBox_FilmID.Text = "Автозаполнение";
            TextBox_FilmFileName.Text = button.ID;
            TextBox_FilmNameEng.Text = button.ID;
            TextBox_FilmNameRus.Text = button.ID;
            TextBox_FilmNameUkr.Text = button.ID;
        }
        else
        {

        }

        if (strSQL != "")
        {
            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = @"Data Source=localhost\MSSQLEXPRESS;Initial Catalog=FilmsDB;Integrated Security=True;Pooling=False";
                cn.Open();

                SqlCommand myCommand = new SqlCommand(strSQL, cn);
                SqlDataReader dr = myCommand.ExecuteReader();

                dr.Read();

                if (dr.VisibleFieldCount == 1)
                {
                    TextBox_FilmID.Text = (dr["FilmId"] != null) ? dr["FilmId"].ToString() : "";
                    TextBox_FilmNameEng.Text = "";
                    TextBox_FilmFileName.Text = "";
                    TextBox_FilmNameRus.Text = "";
                    TextBox_FilmNameUkr.Text = "";
                    TextBox_FilmLangOriginal.Text = "";
                    TextBox_FilmLangDub.Text = "";
                    TextBox_FilmYear.Text = "";
                    TextBox_FilmDirector.Text = "";
                    TextBox_FilmGenre.Text = "";
                    TextBox_FilmRunningTime.Text = "";
                    TextBox_FilmActors.Text = "";
                }
                else if (dr.VisibleFieldCount == 14)
                {
                    TextBox_FilmID.Text = (dr["FilmId"] != null) ? dr["FilmId"].ToString() : "";
                    TextBox_FilmNameEng.Text = (dr["FilmNameEng"] != null) ? dr["FilmNameEng"].ToString() : "";
                    TextBox_FilmFileName.Text = (dr["FilmFileName"] != null) ? dr["FilmFileName"].ToString() : "";
                    TextBox_FilmNameRus.Text = (dr["FilmNameRus"] != null) ? dr["FilmNameRus"].ToString() : "";
                    TextBox_FilmNameUkr.Text = (dr["FilmNameUkr"] != null) ? dr["FilmNameUkr"].ToString() : "";
                    TextBox_FilmLangOriginal.Text = (dr["FilmLangOriginal"] != null) ? ((dr["FilmLangOriginal"].ToString() != "NULL") ? dr["FilmLangOriginal"].ToString() : "") : "";
                    TextBox_FilmLangDub.Text = (dr["FilmLangDub"] != null) ? ((dr["FilmLangDub"].ToString() != "NULL") ? dr["FilmLangDub"].ToString() : "") : "";
                    TextBox_FilmYear.Text = (dr["FilmYear"] != null) ? ((dr["FilmYear"].ToString() != "NULL") ? dr["FilmYear"].ToString() : "") : "";
                    TextBox_FilmDirector.Text = (dr["FilmDirector"] != null) ? ((dr["FilmDirector"].ToString() != "NULL") ? dr["FilmDirector"].ToString() : "") : "";
                    TextBox_FilmGenre.Text = (dr["FilmGenre"] != null) ? ((dr["FilmGenre"].ToString() != "NULL") ? dr["FilmGenre"].ToString() : "") : "";
                    TextBox_FilmRunningTime.Text = (dr["FilmRunningTime"] != null) ? ((dr["FilmRunningTime"].ToString() != "NULL") ? dr["FilmRunningTime"].ToString() : "") : "";
                    TextBox_FilmActors.Text = (dr["FilmActors"] != null) ? ((dr["FilmActors"].ToString() != "NULL") ? dr["FilmActors"].ToString() : "") : "";
                    TextBox_PrevFilmId.Text = (dr["PrevFilmId"] != null) ? ((dr["PrevFilmId"].ToString() != "0") ? dr["PrevFilmId"].ToString() : "") : "";
                    TextBox_NextFilmId.Text = (dr["NextFilmId"] != null) ? ((dr["NextFilmId"].ToString() != "0") ? dr["NextFilmId"].ToString() : "") : "";
                }

                cn.Close();
            }
        }
        else
        {
            TextBox_FilmLangOriginal.Text = "";
            TextBox_FilmLangDub.Text = "";
            TextBox_FilmYear.Text = "";
            TextBox_FilmDirector.Text = "";
            TextBox_FilmGenre.Text = "";
            TextBox_FilmRunningTime.Text = "";
            TextBox_FilmActors.Text = "";
        }

        button = null;
    }

    protected void Button_Save_Click(object sender, EventArgs e)
    {
        Table_DeleteFilm.Visible = false;

        Label_Error.Text = "";

        if (TextBox_FilmFileName.Text == "")
        {
            Label_Error.Text += "Поле \"Имя .mp4 файла (FilmFileName)\" должно быть заполнено!<br>";
        }

        if (TextBox_FilmNameEng.Text == "")
        {
            Label_Error.Text += "Поле \"Название на английском (FilmNameEng)\" должно быть заполнено!<br>";
        }

        if (TextBox_FilmNameRus.Text == "")
        {
            Label_Error.Text += "Поле \"Название на русском (FilmNameRus)\" должно быть заполнено!<br>";
        }

        if (TextBox_FilmNameUkr.Text == "")
        {
            Label_Error.Text += "Поле \"Название на украинском (FilmNameUkr)\" должно быть заполнено!<br>";
        }

        if (TextBox_FilmYear.Text != "")
        {
            int yr;

            if (int.TryParse(TextBox_FilmYear.Text, out yr))
            {
                if (yr < 1900 && yr > DateTime.Now.Year)
                {
                    Label_Error.Text += "Не правильно указано значение поля \"Год (FilmYear)\"!<br>";
                }
            }
            else
            {
                Label_Error.Text += "Не правильно указано значение поля \"Год (FilmYear)\"!<br>";
            }
        }

        if (TextBox_FilmRunningTime.Text != "")
        {
            int rt;

            if (int.TryParse(TextBox_FilmRunningTime.Text, out rt))
            {
                if (!(rt > 0))
                {
                    Label_Error.Text += "Не правильно указано значение поля \"Продолжительность (FilmRunningTime)\"!<br>";
                }
            }
            else
            {
                Label_Error.Text += "Не правильно указано значение поля \"Продолжительность (FilmRunningTime)\"!<br>";
            }
        }

        if (Label_Error.Text != "")
        {
            Label_Error.Text += "Исправьте ошибки перед добавлением данных в базу данных!";
        }
        else
        {
            foreach (var item in Table_FilmList.Rows)
            {
                TableRow tableRow = (TableRow)item;
                Button button = (Button)tableRow.Cells[0].Controls[0];

                if (button.ID == MyStaticClass.selectedFilmName)
                {
                    if (button.BackColor == Color.FromArgb(170, 255, 255))
                    {
                        MyStaticClass.changes = Changes.Update;
                    }
                    else if (button.BackColor == Color.LightGreen)
                    {
                        MyStaticClass.changes = Changes.Save;
                    }
                    else if (button.BackColor == Color.LightPink)
                    {
                        MyStaticClass.changes = Changes.Update;
                    }
                    else
                    {
                        MyStaticClass.changes = Changes.None;
                    }

                    break;
                }
            }
        }

        string strSQL = "";

        if (MyStaticClass.changes == Changes.Save)
        {
            string strSQL_Fields = "FilmNameEng, FilmFileName, FilmNameRus, FilmNameUkr, FilmLangOriginal, FilmLangDub, FilmYear, FilmDirector, FilmGenre, FilmRunningTime, FilmActors, PrevFilmId, NextFilmId";
            string strSQL_Values = "";

            if (TextBox_FilmNameEng.Text != "")
            {
                strSQL_Values += "'" + TextBox_FilmNameEng.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmFileName.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmFileName.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmNameRus.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmNameRus.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmNameUkr.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmNameUkr.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmLangOriginal.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmLangOriginal.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmLangDub.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmLangDub.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmYear.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmYear.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmDirector.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmDirector.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmGenre.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmGenre.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmRunningTime.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmRunningTime.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_FilmActors.Text != "")
            {
                strSQL_Values += ", '" + TextBox_FilmActors.Text + "'";
            }
            else
            {
                strSQL_Values += ", 'NULL'";
            }

            if (TextBox_PrevFilmId.Text != "")
            {
                strSQL_Values += ", '" + TextBox_PrevFilmId.Text + "'";
            }
            else
            {
                strSQL_Values += ", '0'";
            }

            if (TextBox_NextFilmId.Text != "")
            {
                strSQL_Values += ", '" + TextBox_NextFilmId.Text + "'";
            }
            else
            {
                strSQL_Values += ", '0'";
            }

            strSQL = "INSERT INTO FilmTable (" + strSQL_Fields + ") VALUES (" + strSQL_Values + ")";
        }
        else if (MyStaticClass.changes == Changes.Update)
        {
            strSQL = "UPDATE FilmTable SET " +
                "FilmNameEng='" + TextBox_FilmNameEng.Text + "', " +
                "FilmFileName='" + TextBox_FilmFileName.Text + "', " +
                "FilmNameRus='" + TextBox_FilmNameRus.Text + "', " +
                "FilmNameUkr='" + TextBox_FilmNameUkr.Text + "', " +
                "FilmLangOriginal='" + TextBox_FilmLangOriginal.Text + "', " +
                "FilmLangDub='" + TextBox_FilmLangDub.Text + "', " +
                "FilmYear='" + TextBox_FilmYear.Text + "', " +
                "FilmDirector='" + TextBox_FilmDirector.Text + "', " +
                "FilmGenre='" + TextBox_FilmGenre.Text + "', " +
                "FilmRunningTime='" + TextBox_FilmRunningTime.Text + "', " +
                "FilmActors='" + TextBox_FilmActors.Text + "', " +
                "PrevFilmId='" + TextBox_PrevFilmId.Text + "', " +
                "NextFilmId='" + TextBox_NextFilmId.Text + "' " +
                "WHERE FilmId='" + TextBox_FilmID.Text + "'";
        }

        using (SqlConnection cn = new SqlConnection())
        {
            cn.ConnectionString = @"Data Source=localhost\MSSQLEXPRESS;Initial Catalog=FilmsDB;Integrated Security=True;Pooling=False";
            cn.Open();

            SqlCommand myCommand = new SqlCommand(strSQL, cn);
            SqlDataReader dr = myCommand.ExecuteReader();

            dr.Read();

            cn.Close();
        }

        _flag = true;

        HideTables();
    }

    protected void Button_Cancel_Click(object sender, EventArgs e)
    {
        MyStaticClass.selectedFilmName = "";

        MyStaticClass.changes = Changes.None;

        HideTables();
    }

    private void HideTables()
    {
        Label_Error.Text = "";

        Table_ChangeFilmInfo.Visible = false;
        Table_DeleteFilm.Visible = false;
    }

    protected void Button_DeleteFilm_Click(object sender, EventArgs e)
    {
        MyStaticClass.changes = Changes.Delete;

        Table_DeleteFilm.Visible = true;

        Label_FilmName_DeleteFilm.Text = "Вы действительно хотите удалить фильм \"" + MyStaticClass.selectedFilmName + "\"";

        if (TextBox_FilmID.Text == "Автозаполнение" || TextBox_FilmID.Text == "<UKR>" || TextBox_FilmID.Text == "<ENG>")
        {
            Label_FilmName_DeleteFilm.Text += " с диска?";
        }
        else
        {
            Label_FilmName_DeleteFilm.Text += " из базы данных?";
        }
    }

    protected void Button_Delete_Yes_Click(object sender, EventArgs e)
    {
        if (TextBox_FilmID.Text == "Автозаполнение" || TextBox_FilmID.Text == "<UKR>" || TextBox_FilmID.Text == "<ENG>")
        {
            File.Delete(@"D:\inetpub\wwwroot\HomeCinema\Videos\" + MyStaticClass.selectedFilmName + ".mp4");
            File.Delete(@"D:\inetpub\wwwroot\HomeCinema\Videos\" + MyStaticClass.selectedFilmName + ".mp4_eng.jpg");
            File.Delete(@"D:\inetpub\wwwroot\HomeCinema\Videos\" + MyStaticClass.selectedFilmName + ".mp4_eng.txt");
            File.Delete(@"D:\inetpub\wwwroot\HomeCinema\Videos\" + MyStaticClass.selectedFilmName + ".mp4_rus.jpg");
            File.Delete(@"D:\inetpub\wwwroot\HomeCinema\Videos\" + MyStaticClass.selectedFilmName + ".mp4_rus.txt");
            File.Delete(@"D:\inetpub\wwwroot\HomeCinema\Videos\" + MyStaticClass.selectedFilmName + ".mp4_ukr.jpg");
            File.Delete(@"D:\inetpub\wwwroot\HomeCinema\Videos\" + MyStaticClass.selectedFilmName + ".mp4_ukr.txt");
        }
        else
        {
            string strSQL = "DELETE FROM FilmTable WHERE FilmFileName = '" + MyStaticClass.selectedFilmName + "'";

            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = @"Data Source=localhost\MSSQLEXPRESS;Initial Catalog=FilmsDB;Integrated Security=True;Pooling=False";
                cn.Open();

                SqlCommand myCommand = new SqlCommand(strSQL, cn);
                SqlDataReader dr = myCommand.ExecuteReader();

                dr.Read();

                cn.Close();
            }
        }

        _flag = true;

        HideTables();
    }

    protected void Button_Delete_No_Click(object sender, EventArgs e)
    {
        MyStaticClass.changes = Changes.None;

        Table_DeleteFilm.Visible = false;
    }
}
