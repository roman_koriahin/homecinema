﻿using System;
using System.Web.UI;

public partial class AdminMainPage : Page
{
    protected string lang;

    protected string filmsListTXT;
    protected string fileUploaderTXT;
    protected string serverMonitorTXT;
    protected string serverLogTXT;

    protected void Page_Load(object sender, EventArgs e)
    {
        lang = Page.RouteData.Values["lang"] as string;

        if (lang == null)
        {
            lang = Request["lang"];
        }

        if (lang != null)
        {
            switch (lang)
            {
                case "ua":
                    {
                        this.Title = "Адміністраторська";

                        filmsListTXT = "Список фільмів";
                        fileUploaderTXT = "Завантаження файлів на сервер";
                        serverMonitorTXT = "Моніторинг ресурсів сервера";
                        serverLogTXT = "Лог сервера";
                    }
                    break;
                case "ru":
                    {
                        this.Title = "Администраторская";

                        filmsListTXT = "Список фильмов";
                        fileUploaderTXT = "Загрузка файлов на сервер";
                        serverMonitorTXT = "Мониторинг ресурсов сервера";
                        serverLogTXT = "Лог сервера";
                    }
                    break;
                case "en":
                    {
                        this.Title = "Admin page";

                        filmsListTXT = "Film list";
                        fileUploaderTXT = "File uploader";
                        serverMonitorTXT = "Server monitor";
                        serverLogTXT = "Server log";
                    }
                    break;
            }
        }
    }
}