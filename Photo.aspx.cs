﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Windows.Media.Imaging;

public partial class Photo : Page
{
    protected string lang = "";

    public string photoName;
    protected string photoSource;
    public string photoFolderName;

    protected string prevPhotoName;
    protected string nextPhotoName;

    protected bool IsGPSData;

    protected string cameraManufacturer;    // Производитель камеры (271)
    protected string cameraModel;           // Модель камеры (272)
    protected string cameraSoftware;        // Софт камеры (305)
    protected string photoDateTime;         // Дата съёмки (36867)
    protected string exposureTime;          // Время выдержки (33434)
    protected string ISO;                   // Чувствительность ISO (34855)
    protected string fNumber;               // Диафрагма (33437)
    protected string focalLength;           // Фокусное растояние (37386)
    protected string GPSData_DDMMSS;
    protected string GPSData_DDD;

    protected string photoWidth;
    protected string photoHeight;

    private string photoDescription;
    private string fileTXTName;

    private static ASCIIEncoding ASCIIencoding = new ASCIIEncoding();

    protected void Page_Load(object sender, EventArgs e)
    {
        lang = Page.RouteData.Values["lang"] as string;

        if (lang != null)
        {
            TextBox_PhotoDescription.Visible = false;

            Button_Save.Visible = false;
            Button_Cancel.Visible = false;

            photoFolderName = Page.RouteData.Values["album"] as string;
            photoName = Request.QueryString["id"];

            this.Title = photoName;

            string photoFolder = "D:/inetpub/wwwroot/HomeCinema/Images/Photos/" + photoFolderName;

            string[] photos = Directory.GetFiles(@"D:/inetpub/wwwroot/HomeCinema/Images/Photos/" + photoFolderName, "*.jpg").Select(x => Path.GetFileNameWithoutExtension(x)).ToArray();

            prevPhotoName = GetPreviousFile(photos, photoName);
            nextPhotoName = GetNextFile(photos, photoName);

            photoSource = "/Images/Photos/" + photoFolderName + "/" + photoName + ".jpg";

            string photoPath = "D:/inetpub/wwwroot/HomeCinema" + photoSource;

            switch (lang)
            {
                case "ua":
                    {
                        fileTXTName = photoPath + "_ukr.txt";
                    }
                    break;
                case "ru":
                    {
                        fileTXTName = photoPath + "_rus.txt";
                    }
                    break;
                case "en":
                    {
                        fileTXTName = photoPath + "_eng.txt";
                    }
                    break;
            }

            if (File.Exists(fileTXTName))
            {
                photoDescription = File.ReadAllText(fileTXTName);

                if (HttpContext.Current.User.Identity.Name == "Admin")
                {
                    if (photoDescription == null || photoDescription == "" || photoDescription == string.Empty)
                    {
                        photoDescription = "!!! Тут должно быть описание фотографии !!!";

                        Label_PhotoDescription.Text = photoDescription;
                    }
                }
                else
                {
                    Label_PhotoDescription_.Text = photoDescription;
                }
            }
            else
            {
                if (HttpContext.Current.User.Identity.Name == "Admin")
                {
                    photoDescription = "!!! Тут должно быть описание Фотографии !!!";

                    Label_PhotoDescription.Text = photoDescription;
                }
            }

            try
            {
                FileStream photo = File.Open(photoPath, FileMode.Open, FileAccess.Read);

                string extension = photo.Name.Split('.')[1];

                if (extension.ToLower() == "jpg")
                {
                    BitmapDecoder decoder = BitmapDecoder.Create(photo, BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.Default);
                    BitmapMetadata metadata = (BitmapMetadata)decoder.Frames[0].Metadata.Clone();

                    Bitmap bitmap = new Bitmap(photo);

                    if (ReadImageProperty(metadata, "/app1/ifd/{uint=274}") == "6")
                    {
                        MemoryStream ms = new MemoryStream();
                        bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        bitmap.Save(ms, ImageFormat.Jpeg);
                        var base64Data = Convert.ToBase64String(ms.ToArray());
                        photoSource = "data:image/jpeg;base64," + base64Data;

                        photoWidth = bitmap.Width.ToString();
                        photoHeight = bitmap.Height.ToString();

                        bitmap.Dispose();
                    }
                    else if (ReadImageProperty(metadata, "/app1/ifd/{uint=274}") == "8")
                    {
                        MemoryStream ms = new MemoryStream();
                        bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        bitmap.Save(ms, ImageFormat.Jpeg);
                        var base64Data = Convert.ToBase64String(ms.ToArray());
                        photoSource = "data:image/jpeg;base64," + base64Data;

                        photoWidth = bitmap.Width.ToString();
                        photoHeight = bitmap.Height.ToString();

                        bitmap.Dispose();
                    }
                    else
                    {
                        photoWidth = bitmap.Width.ToString();
                        photoHeight = bitmap.Height.ToString();

                        bitmap.Dispose();
                    }

                    cameraManufacturer = ReadImageProperty(metadata, "/app1/ifd/{uint=271}");
                    cameraModel = ReadImageProperty(metadata, "/app1/ifd/{uint=272}");
                    cameraSoftware = ReadImageProperty(metadata, "/app1/ifd/{uint=305}");
                    photoDateTime = metadata.DateTaken;
                    exposureTime = ReadImageProperty(metadata, "/app1/ifd/exif/{uint=33434}");
                    ISO = ReadImageProperty(metadata, "/app1/ifd/exif/{uint=34855}");
                    fNumber = ReadImageProperty(metadata, "/app1/ifd/exif/{uint=33437}");
                    focalLength = ReadImageProperty(metadata, "/app1/ifd/exif/{uint=37386}");
                    GPSData_DDMMSS = GetGPSData(metadata);
                    GPSData_DDD = GetGPSData(metadata, true);

                    photo.Close();
                }

                GC.Collect();
            }
            catch (NotSupportedException)
            {
                throw new NotSupportedException("Ошибка! Возможно файл \"" + photoPath + "\" повреждён или не является изображением!");
            }
        }
    }

    private string GetPreviousFile(string[] photos, string currentFile)
    {
        int count = photos.Length;

        for (int i = 0; i < count; i++)
        {
            if (photos[i] == currentFile)
            {
                if (i != 0)
                {
                    return photos[i - 1];
                }

                break;
            }
        }

        return null;
    }

    private string GetNextFile(string[] photos, string currentFile)
    {
        int count = photos.Length;

        for (int i = 0; i < count; i++)
        {
            if (photos[i] == currentFile)
            {
                if (i != count - 1)
                {
                    return photos[i + 1];
                }

                break;
            }
        }

        return null;
    }

    private string GetGPSData(BitmapMetadata metadata, bool _flag = false)
    {
        string LatitudeRef = (!_flag) ? ReadImageProperty(metadata, "/app1/ifd/gps/{ushort=1}") : "";
        string Latitude = ReadImageProperty(metadata, "/app1/ifd/gps/{ushort=2}", _flag);
        string LongitudeRef = (!_flag) ? ReadImageProperty(metadata, "/app1/ifd/gps/{ushort=3}") : "";
        string Longitude = ReadImageProperty(metadata, "/app1/ifd/gps/{ushort=4}", _flag);

        if (Latitude != "" && Longitude != "")
        {
            IsGPSData = true;

            return Latitude + LatitudeRef + ((!_flag) ? "+" : ", ") + Longitude + LongitudeRef;
        }

        IsGPSData = false;

        return "";
    }

    private string ReadImageProperty(BitmapMetadata metadata, string queryString, bool _flag = false)
    {
        string result = "";

        try
        {
            int ID = GetIDFromQuery(queryString);

            object obj = metadata.GetQuery(queryString);

            switch (obj.GetType().Name)
            {
                case "String":
                    {
                        result = obj.ToString();
                    }
                    break;
                case "String[]":
                    {
                        result = ((string[])obj)[0];
                    }
                    break;
                case "UInt64":
                    {
                        byte[] bb = BitConverter.GetBytes(Convert.ToUInt64(obj));
                        uint uNominator = BitConverter.ToUInt32(bb, 0);
                        uint uDenominator = BitConverter.ToUInt32(bb, 4);

                        if (uDenominator == 1)
                        {
                            result = uNominator.ToString();
                        }

                        if (ID == 33434)
                        {
                            if (uDenominator == 1)
                            {
                                result = uNominator.ToString() + " сек";
                            }
                            else
                            {
                                //result = uNominator.ToString() + "/" + uDenominator.ToString() + " сек";
                                result = "1/" + (uDenominator / uNominator).ToString() + " сек";
                            }
                        }
                        else if (ID == 33437)
                        {
                            result = ("f/" + Math.Round((Convert.ToDouble(uNominator) / Convert.ToDouble(uDenominator)), 1)).Replace(',', '.');
                        }
                        else if (ID == 37386)
                        {
                            result = Math.Round((Convert.ToDouble(uNominator) / Convert.ToDouble(uDenominator)), 2) + " мм";
                        }
                    }
                    break;
                case "UInt64[]":
                    {
                        ulong[] arr = (ulong[])obj;

                        double degrees = ByteArrayToDouble(BitConverter.GetBytes(arr[0]), 0);
                        double minutes = ByteArrayToDouble(BitConverter.GetBytes(arr[1]), 0);
                        double seconds = ByteArrayToDouble(BitConverter.GetBytes(arr[2]), 0);

                        if (!_flag)
                        {
                            result = degrees.ToString() + "°" + minutes.ToString() + "'" + (seconds.ToString()).Replace(',', '.') + "\"";
                        }
                        else
                        {
                            result = (degrees + minutes / 60 + seconds / 3600).ToString().Replace(',', '.');
                        }
                    }
                    break;
                case "UInt16":
                    {
                        result = Convert.ToUInt16(obj).ToString();
                    }
                    break;
                default:
                    {
                        result = "NULL";
                    }
                    break;
            }

        }
        catch
        {
            result = "";
        }

        return result;
    }

    private double ByteArrayToDouble(byte[] bb, int startIdx)
    {
        double result = 0;

        uint uNominator = BitConverter.ToUInt32(bb, startIdx + 0);
        uint uDenominator = BitConverter.ToUInt32(bb, startIdx + 4);

        if (uDenominator == 1)
        {
            result = uNominator;
        }

        result = Convert.ToDouble(uNominator) / Convert.ToDouble(uDenominator);

        return Math.Round(result, 2);
    }

    private int GetIDFromQuery(string queryString)
    {
        int idx1 = queryString.IndexOf('=');
        int idx2 = queryString.IndexOf('}');

        int ID = Convert.ToInt32(new string(queryString.ToCharArray(idx1 + 1, idx2 - idx1 - 1)));

        return ID;
    }

    private string ReadImageProperty(Image image, int ID)
    {
        try
        {
            PropertyItem pi = image.GetPropertyItem(ID);

            if (pi.Type == 2)
            {
                return ASCIIencoding.GetString(pi.Value, 0, pi.Len - 1);

            }
            else if (pi.Type == 3)
            {
                byte[] bb = pi.Value;
                ushort uNUm = BitConverter.ToUInt16(bb, 0);
                return uNUm.ToString();
            }
            else if (pi.Type == 5)
            {
                byte[] bb = pi.Value;
                uint uNominator = BitConverter.ToUInt32(bb, 0);
                uint uDenominator = BitConverter.ToUInt32(bb, 4);

                if (uDenominator == 1)
                {
                    return uNominator.ToString();
                }

                return uNominator.ToString() + "/" + uDenominator.ToString();
            }
            else
            {

            }
        }
        catch
        {

        }

        return "---";
    }

    protected void Button_Edit_Click(object sender, EventArgs e)
    {
        Label_PhotoDescription.Visible = false;
        TextBox_PhotoDescription.Text = Label_PhotoDescription.Text;
        TextBox_PhotoDescription.Visible = true;

        Button_Edit.Visible = false;
        Button_Save.Visible = true;
        Button_Cancel.Visible = true;
    }

    protected void Button_Save_Click(object sender, EventArgs e)
    {
        File.WriteAllText(fileTXTName, TextBox_PhotoDescription.Text, Encoding.UTF8);

        Label_PhotoDescription.Visible = true;
        Label_PhotoDescription.Text = TextBox_PhotoDescription.Text;
        TextBox_PhotoDescription.Visible = false;

        Button_Edit.Visible = true;
        Button_Save.Visible = false;
        Button_Cancel.Visible = false;
    }

    protected void Button_Cancel_Click(object sender, EventArgs e)
    {
        Label_PhotoDescription.Visible = true;
        TextBox_PhotoDescription.Visible = false;

        Button_Edit.Visible = true;
        Button_Save.Visible = false;
        Button_Cancel.Visible = false;
    }
}