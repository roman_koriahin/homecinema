﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilmInListWebUserControl.ascx.cs" Inherits="FilmInListWebUserControl" %>

<table border="1" style="width: 100%;">
    <tr>
        <td rowspan="2" style="width: ">
        <%
            if (Request.Browser.IsMobileDevice)
            {
                %>
                <div style="width: 100px;">
                <%
            }
            else
            {
                %>
                <div style="width: 300px;">
                <%
            }
        %>
                <a href="/<%: lang %>/Film/<%: filmId %>/">
                    <img src="/<%: imgPath %>" alt="" style="width: 100%; overflow: hidden;" />
                </a>
            </div>
        </td>
        <td style="vertical-align: top; height: 40px; width: 100%;">
            <div style="margin-left: 2px; vertical-align: top;">
                <h4>
                    <%
                        if (!_flag)
                        {
                    %>
                    <a href="/<%: lang %>/Film/<%: filmId %>/"><%: filmName %></a>
                    <%
                        }
                        else
                        {
                    %>
                    <a href="/<%: lang %>/Film/<%: filmId %>?do=edit"><%: filmName %></a>
                    <%
                        }
                    %>
                </h4>
            </div>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">
            <div style="margin-left: 2px;"><%: filmDescription %></div>
        </td>
    </tr>
</table>
<br>
