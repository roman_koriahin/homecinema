﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PanoInPhotoListWebUserControl.ascx.cs" Inherits="PanoInPhotoListWebUserControl" %>

<h1>
    <a href="/<%: lang %>/Photos/<%: photoAlbumName %>/Pano?pano=<%: panoHtmlName %>" target="_blank"><%: panoHtmlName %></a>
</h1>
