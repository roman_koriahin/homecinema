﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Pano.aspx.cs" Inherits="Pano" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br>
    <script type="text/javascript">

        function hideUrlBar() {
        }

    </script>
    <style type="text/css" title="Default">
        body, div, h1, h2, h3, span, p {
            font-family: Verdana,Arial,Helvetica,sans-serif;
            color: #000000;
        }

        body {
            font-size: 10pt;
            background: #ffffff;
        }

        h1 {
            font-size: 18pt;
        }

        h2 {
            font-size: 14pt;
        }

        .warning {
            font-weight: bold;
        }

        ::-webkit-scrollbar {
            background-color: rgba(0,0,0,0.5);
            width: 0.75em;
        }

        ::-webkit-scrollbar-thumb {
            background-color: rgba(255,255,255,0.5);
        }
    </style>

    <script type="text/javascript" src="<%: panoSrc %>pano2vr_player.js">
    </script>
    <script type="text/javascript" src="<%: panoSrc %>skin.js">
    </script>
    <table border="1">
        <tr>
            <td>
                <div id="container" style="width: 640px; height: 480px;">
                    This content requires HTML5/CSS3, WebGL, or Adobe Flash Player Version 9 or higher.
                </div>
            </td>
        </tr>
        <tr>
            <td>Тут описание
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        pano = new pano2vrPlayer("container");
        skin = new pano2vrSkin(pano, "<%: panoSrc %>");
        pano.readConfigUrl("<%: panoXmlSrc %>");
        setTimeout(function () { hideUrlBar(); }, 10);
    </script>
    <noscript>
        <p><b>Please enable Javascript!</b></p>
    </noscript>
</asp:Content>

