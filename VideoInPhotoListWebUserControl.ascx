﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VideoInPhotoListWebUserControl.ascx.cs" Inherits="VideoInPhotoListWebUserControl" %>

<h1>
    <a href="/<%: lang %>/Photos/<%: photoAlbumName %>/Video?video=<%: videoName %>" target="_blank"><%: videoName %></a>
</h1>
