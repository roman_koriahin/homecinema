﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PhotoAlbumListWebUserControl : UserControl
{
    public int pageNumber;

    private int photoAlbumsCount;
    private int photoAlbumsOnePage = 10;
    private int pageCount;

    protected void Page_Load(object sender, EventArgs e)
    {
        string lang;

        lang = Page.RouteData.Values["lang"] as string;

        if (pageNumber > 0)
        {
            ReadPhotosFromHDD();

            if (pageCount > 1)
            {
                Table table = new Table();
                table.CellPadding = 4;
                table.GridLines = GridLines.None;
                TableRow tableRowPrev = new TableRow();
                TableCell tableCellPrev = new TableCell();
                tableCellPrev.Text = "<a href=\"/" + lang + "/Photos?page=" + (pageNumber - 1) + "\" class=\"" + ((pageNumber == 1) ? "disabled" : "button") + "\">Пред.</a>";
                tableRowPrev.Cells.Add(tableCellPrev);
                table.Rows.Add(tableRowPrev);

                for (int i = 0; i < pageCount; i++)
                {
                    TableRow tableRow = new TableRow();
                    TableCell tableCell = new TableCell();
                    tableCell.Text = "<a href=\"/" + lang + "/Photos?page=" + (i + 1) + "\" class=\"" + (((i + 1) == pageNumber) ? "currentbutton" : "button") + "\">" + (i + 1) + "</a>";
                    tableRow.Cells.Add(tableCell);
                    table.Rows.Add(tableRow);
                }

                TableRow tableRowNext = new TableRow();
                TableCell tableCellNext = new TableCell();
                tableCellNext.Text = "<a href=\"/" + lang + "/Photos?page=" + (pageNumber + 1) + "\" class=\"" + ((pageNumber == pageCount) ? "disabled" : "button") + "\">След.</a>";
                tableRowNext.Cells.Add(tableCellNext);
                table.Rows.Add(tableRowNext);

                placeHolder1.Controls.Add(table);
            }
        }
    }

    private void ReadPhotosFromHDD()
    {
        DirectoryInfo dirInfo = new DirectoryInfo(@"D:/inetpub/wwwroot/HomeCinema/Images/Photos");

        DirectoryInfo[] photoDirs = dirInfo.GetDirectories();

        photoAlbumsCount = photoDirs.Length;

        List<PhotoAlbumNameToSort> albumName = new List<PhotoAlbumNameToSort>();

        foreach (var photoDir in photoDirs)
        {
            DirectoryInfo photoDirInfo = new DirectoryInfo(photoDir.FullName);

            //FileInfo[] photos = photoDirInfo.GetFiles();

            //long last = 0;

            //foreach (var photo in photos)
            //{
            //    try
            //    {
            //        FileStream photoFile = File.Open(photo.FullName, FileMode.Open, FileAccess.Read);

            //        string extension = photo.Name.Split('.')[1];

            //        if (extension.ToLower() == "jpg")
            //        {
            //            BitmapDecoder decoder = BitmapDecoder.Create(photoFile, BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.Default);
            //            BitmapMetadata metadata = (BitmapMetadata)decoder.Frames[0].Metadata.Clone();

            //            if (metadata.DateTaken == null || metadata.DateTaken == "" || metadata.DateTaken == string.Empty)
            //            {

            //            }
            //            else
            //            {
            //                string createDate = metadata.DateTaken.Split(' ')[0];
            //                string[] createDateArr = createDate.Split('.');

            //                int day = Convert.ToInt32(createDateArr[0]);
            //                int month = Convert.ToInt32(createDateArr[1]);
            //                int year = Convert.ToInt32(createDateArr[2]);

            //                DateTime dt = new DateTime(year, month, day);

            //                if (dt.Ticks > last)
            //                {
            //                    last = dt.Ticks;
            //                }
            //            }
            //        }
            //    }
            //    catch (NotSupportedException)
            //    {
            //        throw new NotSupportedException("Ошибка! Возможно файл \"" + photo.FullName + "\" повреждён или не является изображением!");
            //    }
            //}

            //albumName.Add(new PhotoAlbumNameToSort(last, photoDirInfo.Name));

            albumName.Add(new PhotoAlbumNameToSort(photoDirInfo.Name));
        }

        albumName.Sort();

        int start = 0;
        int end = 0;

        CalculateStartEnd(out start, out end);

        for (int i = start; i <= end; i++)
        {
            PhotoAlbumInListWebUserControl photoAlbumInListWebUserControl = (PhotoAlbumInListWebUserControl)LoadControl("~/PhotoAlbumInListWebUserControl.ascx");
            photoAlbumInListWebUserControl.photoAlbumName = Path.GetFileNameWithoutExtension(albumName[i].albumName);
            placeHolder2.Controls.Add(photoAlbumInListWebUserControl);
        }
    }

    private void CalculateStartEnd(out int start, out int end)
    {
        if (photoAlbumsCount % photoAlbumsOnePage != 0)
        {
            pageCount = ((photoAlbumsCount / photoAlbumsOnePage) * photoAlbumsOnePage + photoAlbumsOnePage) / photoAlbumsOnePage;
        }
        else
        {
            pageCount = photoAlbumsCount / photoAlbumsOnePage;
        }

        start = pageNumber * photoAlbumsOnePage - photoAlbumsOnePage;
        end = pageNumber * photoAlbumsOnePage - 1;

        if (end >= photoAlbumsCount)
        {
            end = photoAlbumsCount - 1;
        }
    }
}
