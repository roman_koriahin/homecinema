﻿using System;
using System.Web.UI;

public partial class FilmInfoWebUserControl : UserControl
{
    public string lang;
    public string filterName;
    public string[] filmInfoArr;

    protected void Page_Load(object sender, EventArgs e)
    {
        int filmInfoArrLength = filmInfoArr.Length;

        for (int i = 0; i < filmInfoArrLength; i++)
        {
            FilmInfoElementWebUserControl filmInfoElementWebUserControl = (FilmInfoElementWebUserControl)LoadControl("~/FilmInfoElementWebUserControl.ascx");
            filmInfoElementWebUserControl.lang = lang;
            filmInfoElementWebUserControl.filterName = filterName;
            filmInfoElementWebUserControl.filterValue = filmInfoArr[i];
            placeHolder.Controls.Add(filmInfoElementWebUserControl);

            if (i < filmInfoArrLength - 1)
            {
                filmInfoElementWebUserControl.IsLastElement = false;
            }
            else
            {
                filmInfoElementWebUserControl.IsLastElement = true;
            }
        }
    }
}