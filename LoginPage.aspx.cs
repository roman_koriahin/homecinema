﻿using System;
using System.Web.Security;
using System.Web.UI;

public partial class LoginPage : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void LoginAction_Click(object sender, EventArgs e)
    {
        Page.Validate();

        if (!Page.IsValid)
        {
            return;
        }

        if (FormsAuthentication.Authenticate(UserName.Text, Password.Text))
        {
            FormsAuthentication.RedirectFromLoginPage(UserName.Text, false);
        }
        else
        {
            LegendStatus.Text = "Вы неправильно ввели имя пользователя или пароль!";
        }
    }
}