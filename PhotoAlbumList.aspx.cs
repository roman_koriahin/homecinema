﻿using System;
using System.Web.UI;

public partial class PhotoAlbumList : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int pageNumber = 1;
        string page = Request.QueryString["page"] as string;

        if (page != null)
        {
            pageNumber = Convert.ToInt32(page);
        }

        this.Title = "Фотографии. Страница " + pageNumber;

        PhotoAlbumListWebUserControl photoAlbumListWebUserControl = (PhotoAlbumListWebUserControl)LoadControl("~/PhotoAlbumListWebUserControl.ascx");
        photoAlbumListWebUserControl.pageNumber = pageNumber;
        PlaceHolder1.Controls.Add(photoAlbumListWebUserControl);
    }
}