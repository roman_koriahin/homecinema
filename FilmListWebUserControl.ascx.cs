﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FilmListWebUserControl : UserControl
{
    public int pageNumber;
    private int filmsCount;
    private int filmsOnePage = 10;
    private int pageCount;

    protected void Page_Load(object sender, EventArgs e)
    {
        string lang = Page.RouteData.Values["lang"] as string;

        if (pageNumber > 0)
        {
            string filterName = Request["filter"];
            string filterValue = Request["value"];

            if (filterName != null && filterName != "" && filterValue != null && filterValue != "")
            {
                switch (filterName)
                {
                    case "langoriginal":
                        {
                            ReadFilmsFromDatabase("FilmLangOriginal", filterValue);
                        }
                        break;
                    case "langdub":
                        {
                            ReadFilmsFromDatabase("FilmLangDub", filterValue);
                        }
                        break;
                    case "year":
                        {
                            ReadFilmsFromDatabase("FilmYear", filterValue);
                        }
                        break;
                    case "director":
                        {
                            ReadFilmsFromDatabase("FilmDirector", filterValue);
                        }
                        break;
                    case "genre":
                        {
                            ReadFilmsFromDatabase("FilmGenre", filterValue);
                        }
                        break;
                    case "actor":
                        {
                            ReadFilmsFromDatabase("FilmActors", filterValue);
                        }
                        break;
                    default:
                        {
                            ReadFilmsFromDatabase();
                        }
                        break;
                }
            }
            else
            {
                ReadFilmsFromDatabase();
            }

            if (pageCount > 1)
            {
                Table table = new Table();
                table.CellPadding = 4;
                table.GridLines = GridLines.None;
                TableRow tableRowPrev = new TableRow();
                TableCell tableCellPrev = new TableCell();
                tableCellPrev.Text = "<a href=\"/" + lang + "/Films?page=" + (pageNumber - 1) + "\" class=\"" + ((pageNumber == 1) ? "disabled" : "button") + "\">Пред.</a>";
                tableRowPrev.Cells.Add(tableCellPrev);
                table.Rows.Add(tableRowPrev);

                for (int i = 0; i < pageCount; i++)
                {
                    TableRow tableRow1 = new TableRow();
                    TableCell tableCell = new TableCell();
                    tableCell.Text = "<a href=\"/" + lang + "/Films?page=" + (i + 1) + "\" class=\"" + (((i + 1) == pageNumber) ? "currentbutton" : "button") + "\">" + (i + 1) + "</a>";
                    tableRow1.Cells.Add(tableCell);
                    table.Rows.Add(tableRow1);
                }

                TableRow tableRowNext = new TableRow();
                TableCell tableCellNext = new TableCell();
                tableCellNext.Text = "<a href=\"/" + lang + "/Films?page=" + (pageNumber + 1) + "\" class=\"" + ((pageNumber == pageCount) ? "disabled" : "button") + "\">След.</a>";
                tableRowNext.Cells.Add(tableCellNext);
                table.Rows.Add(tableRowNext);

                placeHolder1.Controls.Add(table);
            }
        }
    }

    private void ReadFilmsFromDatabase(string filterName = null, string filterValue = null)
    {
        string condition = "";

        if (filterName != null && filterName != "" && filterValue != null && filterValue != "")
        {
            if (filterName == "FilmDirector" || filterName == "FilmGenre" || filterName == "FilmActors")
            {
                condition = " WHERE CHARINDEX('" + filterValue + "', " + filterName + ")>0";
            }
            else
            {
                condition = " WHERE " + filterName + "='" + filterValue + "'";
            }
        }

        string strSQL;
        SqlCommand myCommand;
        SqlDataReader dr;

        using (SqlConnection cn = new SqlConnection())
        {
            cn.ConnectionString = @"Data Source=localhost\MSSQLEXPRESS;Initial Catalog=FilmsDB;Integrated Security=True;Pooling=False";
            cn.Open();

            strSQL = "SELECT COUNT(*) FROM FilmTable" + condition;
            myCommand = new SqlCommand(strSQL, cn);
            dr = myCommand.ExecuteReader();
            dr.Read();
            filmsCount = Convert.ToInt32(dr[0]);
            dr.Close();

            int start = 0;
            int end = 0;

            CalculateStartEnd(out start, out end);

            strSQL = "SELECT * FROM FilmTable" + condition + " ORDER BY FilmNameRus OFFSET " + start + " ROWS FETCH NEXT " + filmsOnePage + " ROWS ONLY";

            myCommand = new SqlCommand(strSQL, cn);
            dr = myCommand.ExecuteReader();

            while (dr.Read())
            {
                FilmInListWebUserControl filmInListWebUserControl = (FilmInListWebUserControl)LoadControl("~/FilmInListWebUserControl.ascx");
                filmInListWebUserControl.filmId = dr["FilmId"].ToString();
                filmInListWebUserControl.filmName = dr["FilmNameRus"].ToString();
                placeHolder2.Controls.Add(filmInListWebUserControl);
            }

            cn.Close();
        }
    }

    private void ReadFilmsFromHDD()
    {
        string[] films = Directory.GetFiles(@"D:/inetpub/wwwroot/HomeCinema/Videos", "*.mp4");

        filmsCount = films.Length;

        int start = 0;
        int end = 0;

        CalculateStartEnd(out start, out end);

        for (int i = start; i <= end; i++)
        {
            FilmInListWebUserControl filmInListWebUserControl = (FilmInListWebUserControl)LoadControl("~/FilmInListWebUserControl.ascx");
            filmInListWebUserControl.filmName = Path.GetFileNameWithoutExtension(films[i]);
            placeHolder1.Controls.Add(filmInListWebUserControl);
        }
    }

    private void CalculateStartEnd(out int start, out int end)
    {
        if (filmsCount % filmsOnePage != 0)
        {
            pageCount = ((filmsCount / filmsOnePage) * filmsOnePage + filmsOnePage) / filmsOnePage;
        }
        else
        {
            pageCount = filmsCount / filmsOnePage;
        }

        start = pageNumber * filmsOnePage - filmsOnePage;
        end = pageNumber * filmsOnePage - 1;

        if (end >= filmsCount)
        {
            end = filmsCount - 1;
        }
    }
}