﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhotoAlbumListWebUserControl.ascx.cs" Inherits="PhotoAlbumListWebUserControl" %>
<%@ Reference Control="~/PhotoAlbumInListWebUserControl.ascx" %>

<table>
    <tr>
        <td>
            <asp:PlaceHolder ID="placeHolder1" runat="server"></asp:PlaceHolder>
        </td>
        <td style="vertical-align:top;">
            <div style="margin-left:10px;">
                <asp:PlaceHolder ID="placeHolder2" runat="server"></asp:PlaceHolder>
            </div>
        </td>
    </tr>
</table>
<br>
