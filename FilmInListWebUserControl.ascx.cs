﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;

public partial class FilmInListWebUserControl : UserControl
{
    public bool _flag;

    public string filmId;
    public string filmName;

    protected string filmDescription = "";
    protected string imgPath = "";

    protected string lang;

    protected void Page_Load(object sender, EventArgs e)
    {
        string fileName;
        string fileTXTName;
        string fileJPGName;

        lang = Page.RouteData.Values["lang"] as string;

        if (lang == null)
        {
            lang = Request["lang"];
        }

        if (lang != null)
        {
            if (!_flag)
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    cn.ConnectionString = @"Data Source=localhost\MSSQLEXPRESS;Initial Catalog=FilmsDB;Integrated Security=True;Pooling=False";
                    cn.Open();

                    string strSQL = "SELECT FilmFileName FROM FilmTable WHERE FilmId='" + filmId + "'";

                    SqlCommand myCommand = new SqlCommand(strSQL, cn);
                    SqlDataReader dr = myCommand.ExecuteReader();

                    dr.Read();

                    fileName = dr["FilmFileName"].ToString();

                    dr.Close();

                    cn.Close();
                }

                fileTXTName = @"D:/inetpub/wwwroot/HomeCinema/Videos/" + fileName;
                fileJPGName = @"Videos/" + fileName;

                switch (lang)
                {
                    case "ua":
                        {
                            fileTXTName += "_ukr.txt";
                            fileJPGName += "_ukr.jpg";
                        }
                        break;
                    case "ru":
                        {
                            fileTXTName += "_rus.txt";
                            fileJPGName += "_rus.jpg";
                        }
                        break;
                    case "en":
                        {
                            fileTXTName += "_eng.txt";
                            fileJPGName += "_eng.jpg";
                        }
                        break;
                }

                if (File.Exists(fileTXTName))
                {
                    filmDescription = File.ReadAllText(fileTXTName);
                }

                imgPath = fileJPGName;
            }
            else
            {
                filmName += ".mp4";
            }
        }
    }
}