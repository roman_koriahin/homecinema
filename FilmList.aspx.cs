﻿using System;
using System.Web.UI;

public partial class FilmList : Page
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        int pageNumber = 1;

        string page = Request.QueryString["page"];

        if (page != null)
        {
            pageNumber = Convert.ToInt32(page);
        }

        this.Title = "Фильмы. Страница " + pageNumber;

        FilmListWebUserControl filmListWebUserControl = (FilmListWebUserControl)LoadControl("~/FilmListWebUserControl.ascx");
        filmListWebUserControl.pageNumber = pageNumber;
        PlaceHolder1.Controls.Add(filmListWebUserControl);
    }
}