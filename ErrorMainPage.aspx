﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ErrorMainPage.aspx.cs" Inherits="ErrorMainPage" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <h1>Извините.</h1>
    <p><span id="errorStatus" runat="server"></span></p>
    <p>
        (Вы задали запрос <strong><span id="errorSrc" runat="server"></span></strong>
        для страницы: <span id="requestedURL" runat="server"></span>)
    </p>
    <p><a href="Default.aspx">Перейти на главную?</a></p>
</asp:Content>
