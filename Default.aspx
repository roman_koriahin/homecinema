﻿<%@ Page Title="Главная страница" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Home Cinema</h1>
    </div>
    <br>
    <%
        switch (lang)
        {
            case "ua":
                {
    %>
    <h1>
        <a href="/ua/Films/">Фільми</a>
    </h1>
    <h1>
        <a href="/ua/Photos/">Фото</a>
    </h1>
    <%
                }
                break;
            case "ru":
                {
    %>
    <h1>
        <a href="/ru/Films/">Фильмы</a>
    </h1>
    <h1>
        <a href="/ru/Photos/">Фото</a>
    </h1>
    <%
                }
                break;
            case "en":
                {
    %>
    <h1>
        <a href="/en/Films/">Films</a>
    </h1>
    <h1>
        <a href="/en/Photos/">Photos</a>
    </h1>
    <%
                }
                break;
        }
    %>
    <br>
</asp:Content>
