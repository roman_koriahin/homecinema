﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Film : Page
{
    protected string lang;

    protected string filmLangOriginal;
    protected string filmNameEng;
    protected string filmLangDub;
    protected string filmNameRus;
    protected string filmNameUkr;
    protected string filmYear;
    protected string filmRunningTime;
    protected string filmSource;
    protected string prevFilmId;
    protected string nextFilmId;
    protected string prevFilmName;
    protected string nextFilmName;
    protected string imgPath = "";

    private string filmId;
    private string filmFileName;
    private string filmDirector;
    private string filmGenre;
    private string filmActors;

    private string filmDescription;

    private string fileTXTName;

    protected void Page_Load(object sender, EventArgs e)
    {
        string fileName;
        string fileJPGName;

        lang = Page.RouteData.Values["lang"] as string;
        filmId = Page.RouteData.Values["id"] as string;

        int a;
        if (int.TryParse(filmId, out a))
        {
            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = @"Data Source=localhost\MSSQLEXPRESS;Initial Catalog=FilmsDB;Integrated Security=True;Pooling=False";
                cn.Open();

                string strSQL = "SELECT * FROM FilmTable WHERE FilmId='" + filmId + "'";
                SqlCommand myCommand = new SqlCommand(strSQL, cn);
                SqlDataReader dr = myCommand.ExecuteReader();

                dr.Read();

                filmNameEng = (dr["FilmNameEng"] != null) ? dr["FilmNameEng"].ToString() : "";
                filmFileName = (dr["FilmFileName"] != null) ? dr["FilmFileName"].ToString() : "";
                filmNameRus = (dr["FilmNameRus"] != null) ? dr["FilmNameRus"].ToString() : "";
                filmNameUkr = (dr["FilmNameUkr"] != null) ? dr["FilmNameUkr"].ToString() : "";
                filmLangOriginal = (dr["FilmLangOriginal"] != null) ? ((dr["FilmLangOriginal"].ToString() != "NULL") ? dr["FilmLangOriginal"].ToString() : "") : "";
                filmLangDub = (dr["FilmLangDub"] != null) ? ((dr["FilmLangDub"].ToString() != "NULL") ? dr["FilmLangDub"].ToString() : "") : "";
                filmYear = (dr["FilmYear"] != null) ? ((dr["FilmYear"].ToString() != "0") ? dr["FilmYear"].ToString() : "") : "";
                filmDirector = (dr["FilmDirector"] != null) ? ((dr["FilmDirector"].ToString() != "NULL") ? dr["FilmDirector"].ToString() : "") : "";
                filmGenre = (dr["FilmGenre"] != null) ? ((dr["FilmGenre"].ToString() != "NULL") ? dr["FilmGenre"].ToString() : "") : "";
                filmRunningTime = (dr["FilmRunningTime"] != null) ? ((dr["FilmRunningTime"].ToString() != "0") ? dr["FilmRunningTime"].ToString() : "") : "";
                filmActors = (dr["FilmActors"] != null) ? ((dr["FilmActors"].ToString() != "NULL") ? dr["FilmActors"].ToString() : "") : "";
                prevFilmId = (dr["PrevFilmId"] != null) ? ((dr["PrevFilmId"].ToString() != "0") ? dr["PrevFilmId"].ToString() : "") : "";
                nextFilmId = (dr["NextFilmId"] != null) ? ((dr["NextFilmId"].ToString() != "0") ? dr["NextFilmId"].ToString() : "") : "";

                dr.Close();

                fileName = filmFileName;

                if (prevFilmId != null && prevFilmId != "" && prevFilmId != "0")
                {
                    strSQL = "SELECT FilmNameRus FROM FilmTable WHERE FilmId='" + prevFilmId + "'";
                    myCommand = new SqlCommand(strSQL, cn);
                    dr = myCommand.ExecuteReader();

                    dr.Read();

                    prevFilmName = dr["FilmNameRus"].ToString();

                    dr.Close();
                }

                if (nextFilmId != null && nextFilmId != "" && nextFilmId != "0")
                {
                    strSQL = "SELECT FilmNameRus FROM FilmTable WHERE FilmId='" + nextFilmId + "'";
                    myCommand = new SqlCommand(strSQL, cn);
                    dr = myCommand.ExecuteReader();

                    dr.Read();

                    nextFilmName = dr["FilmNameRus"].ToString();

                    dr.Close();
                }

                cn.Close();
            }

            fileTXTName = @"D:\inetpub\wwwroot\HomeCinema\Videos\" + fileName;
            fileJPGName = @"Videos\" + fileName;
        }
        else
        {
            fileTXTName = @"D:\inetpub\wwwroot\HomeCinema\Videos\" + filmId;
            fileJPGName = @"Videos\" + filmId;
        }

        switch (lang)
        {
            case "ua":
                {
                    fileTXTName += "_ukr.txt";
                    fileJPGName += "_ukr.jpg";
                }
                break;
            case "ru":
                {
                    fileTXTName += "_rus.txt";
                    fileJPGName += "_rus.jpg";
                }
                break;
            case "en":
                {
                    fileTXTName += "_eng.txt";
                    fileJPGName += "_eng.jpg";
                }
                break;
        }

        if (File.Exists(fileTXTName))
        {
            filmDescription = File.ReadAllText(fileTXTName);

            if (HttpContext.Current.User.Identity.Name == "Admin")
            {
                if (filmDescription == null || filmDescription == "" || filmDescription == string.Empty)
                {
                    filmDescription = "!!! Тут должно быть описание фильма !!!";
                }
            }
        }
        else
        {
            if (HttpContext.Current.User.Identity.Name == "Admin")
            {
                filmDescription = "!!! Тут должно быть описание фильма !!!";
            }
        }

        if (Request.Browser.IsMobileDevice)
        {
            Label_FilmDescription1.Text = filmDescription;
        }
        else
        {
            Label_FilmDescription2.Text = filmDescription;
        }

        imgPath = fileJPGName;

        this.Title = filmNameRus;

        filmSource = "/Videos/" + filmFileName;

        if (int.TryParse(filmId, out a))
        {
            string[] filmDirectorArr = FilmInfoFieldToArray(filmDirector);
            string[] filmGenreArr = FilmInfoFieldToArray(filmGenre);
            string[] filmActorsArr = FilmInfoFieldToArray(filmActors);

            if (Request.Browser.IsMobileDevice)
            {
                SelectPlaceHolder(placeHolderFilmDirector_mob, filmDirectorArr, "director");
                SelectPlaceHolder(placeHolderFilmGenre_mob, filmGenreArr, "genre");
                SelectPlaceHolder(placeHolderFilmActors_mob, filmActorsArr, "actor");
            }
            else
            {
                SelectPlaceHolder(placeHolderFilmDirector_full, filmDirectorArr, "director");
                SelectPlaceHolder(placeHolderFilmGenre_full, filmGenreArr, "genre");
                SelectPlaceHolder(placeHolderFilmActors_full, filmActorsArr, "actor");
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsPostBack)
        {

        }
    }

    private string[] FilmInfoFieldToArray(string filmInfoField)
    {
        return filmInfoField.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
    }

    private void SelectPlaceHolder(PlaceHolder placeHolder, string[] arrayToPalceHolder, string filterName)
    {
        FilmInfoWebUserControl filmInfoWebUserControl = (FilmInfoWebUserControl)LoadControl("~/FilmInfoWebUserControl.ascx");
        filmInfoWebUserControl.lang = lang;
        filmInfoWebUserControl.filterName = filterName;
        filmInfoWebUserControl.filmInfoArr = arrayToPalceHolder;
        placeHolder.Controls.Add(filmInfoWebUserControl);
    }

    protected void Button_Edit_Click(object sender, EventArgs e)
    {
        if (Request.Browser.IsMobileDevice)
        {
            Label_FilmDescription1.Visible = false;
            //TextBox_FilmDescription1.Visible = true;
        }
        else
        {
            TextBox_FilmDescription2.Text = Label_FilmDescription2.Text;

            Label_FilmDescription2.Visible = false;
            TextBox_FilmDescription2.Visible = true;
        }
    }

    protected void Button_Save_Click(object sender, EventArgs e)
    {
        if (Request.Browser.IsMobileDevice)
        {
            Label_FilmDescription1.Visible = true;
            //TextBox_FilmDescription1.Visible = false;

            //File.WriteAllText(fileTXTName, TextBox_FilmDescription1.Text);
        }
        else
        {
            Label_FilmDescription2.Text = TextBox_FilmDescription2.Text;

            File.WriteAllText(fileTXTName, TextBox_FilmDescription2.Text);

            Label_FilmDescription2.Visible = true;
            TextBox_FilmDescription2.Visible = false;
        }
    }

    protected void Button_Cancel_Click(object sender, EventArgs e)
    {
        if (Request.Browser.IsMobileDevice)
        {
            Label_FilmDescription1.Visible = true;
            //TextBox_FilmDescription1.Visible = false;
        }
        else
        {
            Label_FilmDescription2.Visible = true;
            TextBox_FilmDescription2.Visible = false;
        }
    }
}
