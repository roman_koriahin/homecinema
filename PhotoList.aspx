﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="PhotoList.aspx.cs" Inherits="PhotoList" %>

<%@ Register TagPrefix="photoListWebUserControl" TagName="PhotoListWebUserControl" Src="~/PhotoListWebUserControl.ascx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1><%: photoAlbumName.Replace("_", ".") %></h1>
    <br>
    <asp:PlaceHolder ID="placeHolder1" runat="server"></asp:PlaceHolder>
    <br>
</asp:Content>
