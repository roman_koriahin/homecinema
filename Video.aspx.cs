﻿using System;
using System.Web.UI;

public partial class Video : Page
{
    private string photoAlbumName;

    protected string videoName;
    protected string videoSource;

    protected void Page_Load(object sender, EventArgs e)
    {
        videoName = Request["video"];
        photoAlbumName = Page.RouteData.Values["album"] as string;

        videoSource = @"/images/Photos/" + photoAlbumName + @"/" + videoName + ".mp4";

        this.Title = videoName;
    }
}