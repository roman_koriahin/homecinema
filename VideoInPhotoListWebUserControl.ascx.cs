﻿using System;
using System.Web.UI;

public partial class VideoInPhotoListWebUserControl : UserControl
{
    public string photoAlbumName;
    public string videoName;

    protected string lang;

    protected void Page_Load(object sender, EventArgs e)
    {
        lang = Page.RouteData.Values["lang"] as string;
    }
}