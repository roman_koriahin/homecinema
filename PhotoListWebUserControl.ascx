﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhotoListWebUserControl.ascx.cs" Inherits="PhotoListWebUserControl" %>
<%@ Reference Control="~/PanoInPhotoListWebUserControl.ascx" %>
<%@ Reference Control="~/VideoInPhotoListWebUserControl.ascx" %>

<table>
    <tr>
        <td>
            <asp:PlaceHolder ID="placeHolder1" runat="server"></asp:PlaceHolder>
        </td>
        <td style="vertical-align:top;">
            <div style="margin-left:10px;">
                <asp:PlaceHolder ID="placeHolder2" runat="server"></asp:PlaceHolder>
            </div>
        </td>
    </tr>
</table>
<br>
